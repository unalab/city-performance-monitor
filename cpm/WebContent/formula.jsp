<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="main.java.Queries"%>
<%@page import="java.util.ArrayList"%>

<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>


  
 <% String role = (String) request.getSession().getAttribute("user");
  String [] array = role.split("_");
  String c= array[0];
  Integer id = Queries.getCityId(c);
	
%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		

<link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<link type="text/css" rel="stylesheet" href="css/customEngine.css" media="screen,projection" />
<link type="text/css" rel="stylesheet" href="css/codemirror.css" media="screen,projection" /> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  

	


				
</head>

<% //In case, if Admin session is not set, redirect to Login page
if((request.getSession(false).getAttribute("user")== null) )
{
%>
<jsp:forward page="login.jsp"></jsp:forward>
<%} 


%>

<body>

<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo" style="width: 42%"></a> 
		<span id="titleCity" class="brand-logo secondary-text" style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>
		<span class="text_center hide">
			<span style="text-align: center" id="testo"></span>
		</span>

		<ul class="right">
			
			<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<% if (role == "admin"){%>
			
			<li><a href="/cpm_v2/GetAdminKPIList?city=3&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
			
		<%} else {
			
			
			%>		
			
			<li><a href="/cpm_v2/view.jsp?city=<%=id%>&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
 		 
	<% } %>
			
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/view.jsp?city=${param.city}&level=task&category=all&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			
				<span class="val"><%=c%>-admin</span> 
			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
			
		</ul>
	</div>
	</nav>
	
	
<nav class="clean" id="bread">
    <div class="nav-wrapper">
      <div class="col s12">
        <a id="measureO" class="breadcrumb">Select measures</a>
        <a id="formulaO" class="breadcrumb selectedBread">Build KPI Formula</a>
        <a id="scheduleO" class="breadcrumb">Schedule KPI Calculation</a>
      </div>
    </div>
  </nav>
  
  
     <div class="row">
    
     
     	<div id="measures" class="col s5">
   			<h5 class="center">Measures</h5>
<!--       		<div class="card-panel measureCard"> -->
<!--         		<div class="card-content black-text"> -->
          		
<!--           	     	<fieldset> -->
    
<!--     					<div class="chip-group" id="chips" tabindex="-1"> -->
      						
      						
<!--       					</div> -->
<!--   					</fieldset> -->
      
<!--      			</div> -->
   
<!--      		</div> -->
			<input style="width:50%; margin-left:25%" class="unalabInput" type="text" id="search2" placeholder="Type to search measures"></input>
     		<div class="collection collectionMeasures" style="width:50%; margin-left:25%">
        
      		</div>
<!--      		 <form action="" class="browser-default right"> -->
<!--         <input id="search-input" placeholder="Search measures" type="text" class="browser-default search-field" name="q" value="" autocomplete="off" aria-label="Search box"> -->
<!--          <label for="search-input"> -->
<!--          <i class="material-icons search-icon">search</i> -->
<!--          </label>   -->
<!--          <i class="material-icons search-close-icon">cancel</i>  -->
<!--         <div class="search-popup"> -->
<!--           <div class="search-content"> -->
<!--             <label class="search-heading">Most Recent</label> -->
            
<!-- <!--             <ul class="popup-list"> --> 
              
<!-- <!--             </ul> --> 
<!--           </div> -->
<!--         </div> -->
<!--       </form> -->
     		
<!--      		<div id="operators" class=""> -->
<!--    			<h4 class="center">Operators</h4> -->
<!--       		<div class="card-panel operatorsCard"> -->
<!--         		<div class="card-content black-text"> -->
          		
<!--           	     	<fieldset> -->
    
<!--     					<div class="chip-group" id="ops" tabindex="-1"> -->
      						
<!--       						<div class="chip chip-checkbox" aria-labelledby="sum" tabindex="0" role="checkbox" aria-checked="false"> -->
<!--         						<input type="checkbox" name="checkEx2" /> -->
<!--         						<span id="sum">SUM</span> -->
<!--         					</div> -->
        					
<!--         					<div class="chip chip-checkbox" aria-labelledby="avg" tabindex="0" role="checkbox" aria-checked="false"> -->
<!--         						<input type="checkbox" name="checkEx2" /> -->
<!--         						<span id="avg">AVG</span> -->
<!--         					</div> -->
        					
<!--         					<div class="chip chip-checkbox" aria-labelledby="min" tabindex="0" role="checkbox" aria-checked="false"> -->
<!--         						<input type="checkbox" name="checkEx2" /> -->
<!--         						<span id="min">MIN</span> -->
<!--         					</div> -->
        					
<!--         					<div class="chip chip-checkbox" aria-labelledby="max" tabindex="0" role="checkbox" aria-checked="false"> -->
<!--         						<input type="checkbox" name="checkEx2" /> -->
<!--         						<span id="max">MAX</span> -->
<!--         					</div> -->
      						
<!--       						<div class="chip chip-checkbox" aria-labelledby="count" tabindex="0" role="checkbox" aria-checked="false"> -->
<!--         						<input type="checkbox" name="checkEx2" /> -->
<!--         						<span id="count">COUNT</span> -->
<!--         					</div> -->
      						
<!--       					</div> -->
<!--   					</fieldset> -->
      
<!--      			</div> -->
   
<!--      		</div> -->
<!--   		</div> -->
  		</div>
  		
  		
  		<div id="textarea" class="col s7">
  		  		
  			<div class="wrapper">
  			<fieldset id="operatori">
    					
    					<div class="chip-group" id="ops" tabindex="-1">
      						<h5 id="operatorsTitle">Operators (drag and drop) :     </h5>
      						<div class="chip chip-checkbox" aria-labelledby="sum" tabindex="0" role="checkbox" aria-checked="false">
        						<input type="checkbox" name="checkEx2" />
        						<span id="sum">SUM</span>
        					</div>
        					
        					<div class="chip chip-checkbox" aria-labelledby="avg" tabindex="0" role="checkbox" aria-checked="false">
        						<input type="checkbox" name="checkEx2" />
        						<span id="avg">AVG</span>
        					</div>
        					
        					<div class="chip chip-checkbox" aria-labelledby="min" tabindex="0" role="checkbox" aria-checked="false">
        						<input type="checkbox" name="checkEx2" />
        						<span id="min">MIN</span>
        					</div>
        					
        					<div class="chip chip-checkbox" aria-labelledby="max" tabindex="0" role="checkbox" aria-checked="false">
        						<input type="checkbox" name="checkEx2" />
        						<span id="max">MAX</span>
        					</div>
      						
      						<div class="chip chip-checkbox" aria-labelledby="count" tabindex="0" role="checkbox" aria-checked="false">
        						<input type="checkbox" name="checkEx2" />
        						<span id="count">COUNT</span>
        					</div>
      						
      					</div>
  					</fieldset>
  			 	<textarea id="formula"></textarea>
				<div id="saveFormula">
					<a class="btn btn-right formulabtn modal-trigger" href="#saveRES">Save Formula</a>
				</div>
				<div id="preview">
					<a class="btn btn-right formulabtn modal-trigger" href="#modalRES" onclick="getFormula()">Preview</a>
				</div>
  			</div>	
		</div>
  		
  	</div>
  	
  	<div id="modalRES" class="modal">
  <div class="modal-content">
    <h5>Formula Result Preview</h5>
    <p class="ris"></p>
  </div>
  <div class="modal-footer">
    <a href="#!" class=" modal-action modal-close waves-effect waves-green btn-flat">OK</a>
  </div>
</div>

<div id="saveRES" class="modal">
    <div class="modal-content">
      
      
      <div class="input-field col s3 ">
      	<input value="" id="formulaName" type="text" class="validate formulaName">
      	<label class="active" for="measure">Insert a name for this formula</label>
   	 </div>
      
    </div>
    <div class="modal-footer">
      <a onclick="saveFormula()" class="modal-close waves-effect waves-green btn-flat">OK</a>
    </div>
  </div>
  	
  </body>	
  	
<script type="text/javascript" src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjs/6.6.0/math.min.js"></script>
<script type="text/javascript" src="js/formula.js"></script>
  