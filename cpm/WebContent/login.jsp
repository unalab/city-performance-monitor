<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> -->
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>
<script type="text/javascript" src="js/custom.js"></script>
 <script type="text/javascript" src="js/paginathing.js"></script>  
<script type="text/javascript" src="js/admin.js"></script>




				
</head>
<body>
<div class="container">
	<div class="row">
	<div class="section"></div>
   <main>
    <center>
     <div class="container">
        <div  class="z-depth-3 y-depth-3 x-depth-3 grey green-text lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px; margin-top: 100px; solid #EEE;">
        
		<form action="/cpm_v2/LoginServlet?lang=en" method="post" >  
    
      <div class="section"><i class="mdi-alert-error red-text"></i></div>
         
          <a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 82%"></a>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type="email" name='username' id='username' required />
                <label for='username'>E-Mail</label>
              </div>
            </div>
            <div class='row'>
              <div class='input-field col m12'>
                <input class='validate' type='password' name='password' id='password' required />
                <label for='password'>Password</label>
              </div>
              <label style='float: right;'>
              <a><b style="color: #F5F5F5;">Forgot Password?</b></a>
              </label>
            </div>
            <br/>
            <center>
              <div class='row'>
                <button style="margin-left:65px;"  type='submit' name='btn_login' class='col  s6 btn btn-small waves-effect z-depth-1 y-depth-1'>Login</button>
              </div>
            </center>
            
         </form>  
     
        </div>
       </div>
      </center>
      </main>
   

	</div>
</div>

<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div>
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px;">&copy; UNALAB project</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>
</body>



<script>

function getInitialKPI(){

	console.log("CHIAMA FUNZIONE");
	var input = new Object();
	var username = $('#username').val();
	var password = $('#password').val();
	input['city'] = 3;
	input['username'] = username;
	input['password'] = password;
	
	$('#myTable').empty();
	$.ajax({
		async: false,
        type: "POST",
        url: "LoginServlet",
        data: input,
        success: function(json){
        	
					data = JSON.parse(json);
					for (var i = 0; i< data.length; i++){
						var row= $('<tr></tr>').appendTo('#myTable');
						row.addClass(data[i].icon+"Class");
						var id = $('<th scope="row"></th>').appendTo(row);
						id.text(data[i].tableId);
						var category=$('<td style="text-align:center"></td>').appendTo(row);
						var catIcon = $('<i class="" aria-hidden="true"></i>').appendTo(category);
						catIcon.addClass("fa fa-"+data[i].icon);
						var columnName = $('<td id="title"></td>').appendTo(row);
						//var icon = $('').appendTo(columnName);
						columnName.text(data[i].name+ " ");
						
						var desc = $('<i class="fa fa-info-circle" aria-hidden="true" data-toggle="tooltip"></i>').appendTo(columnName);
						desc.attr('title', data[i].description);
						$('[data-toggle="tooltip"]').tooltip({title: ''});

						var values = $('<td></td>').appendTo(row);
						values.text(data[i].value);

						var unit = $('<td></td>').appendTo(row);
						unit.text(data[i].unit);

						var date= $('<td></td>').appendTo(row);
						date.text(data[i].date);
						var action1= $('<td></td>').appendTo(row);
						
						var chart = $('<a href=#cont></a>').appendTo(action1);
						var actionIcon = $('<i class="fa fa-line-chart unalabGreen3" aria-hidden="true" data-toggle="tooltip" title="View Indicator"></i>').appendTo(chart);
						actionIcon.attr("id", "icon_"+data[i].idKPI);
						console.log(data[i].name+", " +data[i].isManual);
						
							chart.attr('onclick', "dashboardParamsExpert("+data[i].idKPI+"," +data[i].idCity+")");
							
						
					}

					 $("#myTable").paginathing({
              
              			perPage: 10
            
					});
        }
	});

}

</script>