<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>
<%@page import="java.util.ArrayList"%>
<%@page import="main.java.Queries"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%String role = (String) request.getSession().getAttribute("user");
String [] array; 
String c; 
Integer id;%>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
		
<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
  <link type="text/css" rel="stylesheet" href="css/common.css" media="screen,projection" /> 
 <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
<!--   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css"> -->
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!--     <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/paginathing.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/table-sorter.js"></script>
<script type="text/javascript" src="js/custom.js"></script>




				
</head>
<body>
<%-- <c:import url="/GetKPIList2" /> --%>
<c:set var="city" value="${param.city}" />
<% if(role != null) {
	array = role.split("_");
	c = array[0];
	id = Queries.getCityId(c);
	%>
	<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>

<span class="text_center hide">
				 	<span style="text-align: center" id="testo"></span>
				 </span>

		<ul class="right">
		       <li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<% if (role == "admin"){%>
				<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
				<li><a href="/cpm_v2/GetAdminKPIList?city=3&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
			
			<%} else {%>	
				<li><a href="/cpm_v2/GetCommonKPIs?city=<%=id%>&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
				<li><a href="/cpm_v2/view.jsp?city=<%=id%>&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
				
			<% } %>
			
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right">
			
			<a class="username dropdown-button dropdown-user-big" href="#" data-target="userdropdown_big">
			<i class="material-icons left">perm_identity</i>
			<%if (role == "admin"){%>
				<span class="val">cpm-admin</span> 
			<% } else { %>
				<span class="val"><%=c%>-admin</span> 
			
			<%} %>			
			<i class="material-icons right">arrow_drop_down</i>
			</a>
			<ul id="userdropdown_big"  class='dropdown-content'>

				<li><a href="/cpm_v2/LogoutServlet"><i18n:message value="logout"/></a></li>

			</ul>
			</li>
		</ul>
	</div>
	</nav>
	<%} else{%>
	<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>

<span class="text_center hide">
				 	<span style="text-align: center" id="testo"></span>
				 </span>
		<ul class="right">
			<li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4">Help</a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<li><a href="/cpm_v2/guest.jsp?city=${param.city}&level=task&category=all&lang=${lang}" class="white-text text-darken-4"><i18n:message value="expert"/></a></li>
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right"><a class="username dropdown-user-big" href="/cpm_v2/login.jsp"><i
					class="material-icons left">input</i><span class="val"><i18n:message value="login"/></span> </a></li>
		</ul>
	</div>
	</nav>
	<%} %>
	
	<div style="margin:20px 10px">
	<iframe id="cityDashboard" width="100%" height="800"
    src=""
     frameborder="0">
	</iframe>
	</div>
	
	
<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
<!-- 		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div> -->
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px; float: left;">&copy; 2019 UNaLab All rights reserved</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>
	