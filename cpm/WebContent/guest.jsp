<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page import="main.java.KPIBean"%>
<%@page import="main.bean.LoginBean"%>

<%@page import="java.util.ArrayList"%>
<%@taglib uri="http://eng.it/ricerca/pa/taglib/i18n" prefix="i18n"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%-- <% ArrayList<KPIBean> kpiList = new ArrayList<KPIBean>(); --%>
<!-- kpiList = (ArrayList<KPIBean>) request.getAttribute("kpiList"); -->
  
  
	
<%-- %> --%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta charset="ISO-8859-1">
	
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />

	<title>City Performance Monitor</title> 
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">

<link type="text/css" rel="stylesheet" href="css/table-sorter.css" media="screen,projection" /> 		
 <link type="text/css" rel="stylesheet" href="css/custom.css" media="screen,projection" /> 
<link type="text/css" rel="stylesheet" href="css/common.css" media="screen,projection" /> 
<!--  <link type="text/css" rel="stylesheet" href="css/materialize.css" media="screen,projection" />  -->
  <link type="text/css" rel="stylesheet" href="css/materialize.min.css" media="screen,projection" /> 
  <link type="text/css" rel="stylesheet" href="css/theme.default.css" media="screen,projection" /> 
<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/css/materialize.min.css">-->







</head>


<body>
<%-- <c:import url="/GetKPIList2" /> --%>
<c:set var="city" value="${param.city}" />


	<nav id="header">
	<div class="nav-wrapper customW">
		<a href="index.jsp" class="brand-logo"><img
			src="img/logo_alpha_270x.png" class="logo" alt="UNaLab logo"
			style="width: 42%"></a> <span id="titleCity" class="brand-logo secondary-text"
			style="margin-left: 130px; margin-top: 12px; color: #77943E"></span>

	<span class="text_center hide">
				 	<span style="text-align: center" id="testo"></span>
				 </span>

		<ul class="right">
		    <li><a id="dwn-btn" class="white-text text-darken-4" onClick="generateJson(${param.city})">Download indicators</a></li>
		    
		    <li><a href="http://unalab.eng.it/cpm-user-guide/build/html/" class="white-text text-darken-4"><i18n:message value="help"/></a></li>
			<li><a href="/cpm_v2/dashboard.jsp?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citydashboard"/></a></li>
			<li><a href="/cpm_v2/GetCommonKPIs?city=${param.city}&lang=${lang}" class="white-text text-darken-4"><i18n:message value="citizenview"/></a></li>
			<li>
				<a class="username dropdown-button dropdown-user-big" data-constrainwidth="false" data-target="languagemenu_big">
					<span class="activeLang ">${lang}</span>
					<i class="material-icons left ">language</i>
					<i class="material-icons right">arrow_drop_down</i>
				</a>
			<ul id="languagemenu_big"  class='dropdown-content'>

				<li>
				<a class="" href="/cpm_v2/guest.jsp?city=${param.city}&level=task&category=all&lang=en">
					<i id="en" class="inline material-icons hide">done</i>
					
					<i18n:message value="english"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/guest.jsp?city=${param.city}&level=task&category=all&lang=it">
					<i id="it" class="inline material-icons hide">done</i>
					
					<i18n:message value="italian"/>
				</a>
			</li>
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/guest.jsp?city=${param.city}&level=task&category=all&lang=fi">
					<i id="fi" class="inline material-icons hide">done</i>
					<i18n:message value="finnish"/>
					
				</a>
			</li>
			
			<li class="divider"></li>
			<li>
				<a class="" href="/cpm_v2/guest.jsp?city=${param.city}&level=task&category=all&lang=du">
					<i id="du" class="inline material-icons hide">done</i>
					<i18n:message value="dutch"/>
						
				</a>
			</li>
		

			</ul>
			</li>
			<li class="right"><a class="username dropdown-user-big" href="/cpm_v2/login.jsp"><i
					class="material-icons left">input</i><span class="val"><i18n:message value="login"/></span> </a></li>
			
		</ul>
	</div>
	</nav>
	
	
<div class = "row">

<div class="col s12">
<h5 class="center" ><i18n:message value="expert"/></h5>
<div id="transitCard" style="display:none;"></div>

</div>
</div>

	<div class="row">
	
	
<div class="col s12 m12 l3">
      
        <div class="nav-wrapper">
        
<%--         <form id="selectForm" action="/cpm/GetGuestKPIList?city=${param.city}&lang=${lang}" --%>
<!-- 			 method="post">	   -->
      
<input type="hidden" name="city" class="city" value="${param.city}"></input>  

<ul class="collapsible" data-collapsible="expandable"> 

	<li>
    	<div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="level"/></div>
        <div class="collapsible-body filter-container">
			<ul class="collection">
		    	<li class="collection-item" style="border-bottom: 0px;">
 					<label>
   						<input class="radiobtn with-gap" name="radio1" type="radio" id="radio1"  value="common" checked/>
    					<span class="radio1">Common Indicators</span>
  					</label>
  				</li>
  				
				<li class="collection-item" style="border-bottom: 0px;">
  					<label>
    					<input class="radiobtn with-gap" name="radio1" type="radio" id="radio2" value="city" />
    					<span class="radio1">City-Level indicators
    				</label>
    			</li>
    			
    			<li class="collection-item" style="border-bottom: 0px;">
    				<label>
    					<input class="radiobtn with-gap" name="radio1" type="radio" id="radio3" value="project" />
    					<span class="radio1">Project-Level Indicators
    				</label>
    			</li>
    		</ul>
  
  		</div>
	</li> <!--  chiuso li collapsible -->
</ul> <!--  chiuso ul collapsible -->
<!-- </form> -->
<ul class="collapsible" data-collapsible="expandable" id="collapsibleOne" > 

         <li>
          <div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="category"/></div>
          <div class="collapsible-body filter-container">

 <ul>
       <li style="padding: 2px !important">
    			<p>
      			<label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="cloud_queue" value="climate" />
                <span class="cloud_queue"> <i class="material-icons small left" style="font-size:1.5rem">cloud_queue</i> <i18n:message value="climate"/></label>
              
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one  filled-in" checked="checked" id="warning" value="airquality"  />
                <span class="warning"> <i class="material-icons small left" style="font-size:1.5rem">warning</i><i18n:message value="airquality"/></label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="euro_symbol" value="economic"/>
                <span class="euro_symbol"><i class="material-icons small left" style="font-size:1.5rem">euro_symbol</i> <i18n:message value="economic"/></label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="waves" value="water" />
                <span class="waves"><i class="material-icons small left" style="font-size:1.5rem">waves</i><i18n:message value="water"/></label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
      <label>
                <input type="checkbox" name="event-type-filter" class="event-type-filter-one filled-in" checked="checked" id="nature_people" value="green" />
                <span class="nature_people"><i class="material-icons small left" style="font-size:1.5rem">nature_people</i><i18n:message value="green"/></label>
              </label>
              </p>
              </li>
              
            </ul>
	</div>
	</li>
	</ul>


<ul class="collapsible" style="display:none;" data-collapsible="expandable" id="collapsibleTwo" > 
 <li>
          <div class="collapsible-header active managerFilter"><i class="material-icons">label</i><i18n:message value="category"/></div>
          <div class="collapsible-body filter-container">

 <ul>
              <li style="padding: 2px !important">
              
             <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="group" value="people" />
                <span class="group"> <i class="material-icons small left" style="font-size:1.5rem">group</i>People</label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two  filled-in" checked="checked" id="public" value="airquality"  />
                <span class="public"> <i class="material-icons small left" style="font-size:1.5rem">public</i>Planet</label>
             </label>
             </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="attach_money" value="economic"/>
                <span class="attach_money"><i class="material-icons small left" style="font-size:1.5rem">attach_money</i> Prosperity</label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="graphic_eq" value="water" />
                <span class="graphic_eq"><i class="material-icons small left" style="font-size:1.5rem">graphic_eq</i>Propagation</label>
              </label>
              </p>
              </li>
              <li style="padding: 2px !important">
              <p>
             <label>
                <input type="checkbox" name="event-type-filter2" class="event-type-filter-two filled-in" checked="checked" id="gavel" value="green" />
                <span class="gavel"><i class="material-icons small left" style="font-size:1.5rem">gavel</i>Governance</label>
              </label>
              </p>
              </li>
            </ul>
	</div>
	</li>
	</ul>


</div>
  </div>

				

	
		<div class="col s9" style="padding-left:35px">
			<input class="unalabInput" type="text" id="search" placeholder="<i18n:message value="typetosearch"/>" />
			<div class="tableContent">
			<table cellpadding="1" cellspacing="1" class="table table-hover sortable-table indexed tablesorter" id="myTable2">
				<thead>
					<tr class="sorter-header">
						<th onclick="sortTable(0)" class="unalabTd no-sort"><u>ID</u></th>
						<th onclick="sortTable(1)" class="unalabTd no-sort"><u><i18n:message value="indicator"/></u></th>
						<th onclick="sortTable(2)" class="unalabTd no-sort" id="nbsTitle"><u>NBS title</u></th>
						<th class="unalabTd no-sort">Category</th>

						<th class="unalabTd no-sort"><i18n:message value="value"/></th>
						<th onclick="sortTable(5)"class="unalabTd no-sort"><u><i18n:message value="unit"/></u></th>
						
						<th onclick="sortTable(6)" class="unalabTd is-date"><u>Last Update</u></th>
						<th class="unalabTd no-sort"><i18n:message value="actions"/></th>

					</tr>
				</thead>
				<tbody id="myTable">
		
			
			
				</tbody>
			</table>
			</div>
			
			
<!-- 			<div id="pag" class="col-md-12 center text-center" style="position:fixed;bottom:30px;left:0;width:100%;"> -->
<!-- 				<span class="center" id="total_reg"></span> -->
<!-- 				<ul class="pagination pager" id="myPager"></ul> -->
<!-- 			</div> -->
		</div>
		
		

	</div>


<footer class="valign-wrapper primary"
		style="position:fixed;bottom:0;left:0;width:100%;">
<!-- 		<div class="footer-section section-left white-text" style="background-color: black; width: 100%; height: 35px;">&nbsp;</div> -->
	<div class="footer-section section-center white-text" style="background-color: black; width: 100%; height: 35px; float: left;">&copy; 2019 UNaLab All rights reserved</div>
		
		<div class="footer-section section-right white-text" style="background-color: black; width: 100%; height: 35px;"><span>Powered by </span><img class="footer-logo" src="img/digital-enabler-white.png"/></div>
	</footer>

<!-- Modal Structure -->
  <div id="modal1" class="modal">
    <div class="modal-content">
    
<!--     <div class="switch center"> -->
<!--     <label> -->
<!--       Manual Indicator -->
<!--       <input id="manualComputedSwitch" type="checkbox"> -->
<!--       <span class="lever customSwitch"></span> -->
<!--       Computed indicator -->
<!--     </label> -->
<!--   </div> -->
    <div id="manualForm">
      <h5>Add a new manual indicator</h5>

      <div class="row">
        <form action="/cpm_v2/AddKPI" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="id" name="id" type="number" class="validate unalabInput" required>
              <label class="unalabLabel" for="id">ID</label>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="name" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label for="name">Name</label>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="description" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label for="description">Description</label>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unit" type="text" name="unit" class="validate unalabInput" required>
              <label for="unit">Unit Measure</label>
            </div>
          </div>     
          <div class="row modal-form-row">  
             
			<div class="input-field col s12">
						<input name="city" type="hidden" value="${param.city}"></input> <span>Filter by Indicators level</span> 
						<select
							id="level" name="level">
							<option value="task">Common Indicators</option>
							<option value="city">City-Level Indicators</option>
							<option value="project">Project-Level Indicators</option>
						</select> 
							
							</div>
							
							<div class="input-field col s12">
							<span>Filter by
							Indicators category</span> 
							<select id="cat1" name="cat1">

							<option value="climate">Climate change adaptation and mitigation</option>
							<option value="airquality">Air Quality</option>
							<option value="economic">Economic opportunities and green jobs</option>
							<option value="water">Water management</option>
							<option value="green">Green space management</option>

						</select> 
						<select id="cat2" name="cat2">

							<option value="people">People</option>
							<option value="planet">Planet</option>
							<option value="prosperity">Prosperity</option>
							<option value="governance">Governance</option>
							<option value="propagation">Propagation</option>


						</select>
						</div>
						</div>
						
						 <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Add</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>
						
				</form>
      </div>
      </div>
      
      
      <!-- Computed Indicator Adding Form -->
      <div id="computedForm">
      <h5>Add a new computed indicator</h5>

      <div class="row">
        <form action="/cpm_v2/AddComputedKPI" method="post">
        
          <div class="row modal-form-row">
          
          <div class="dropdownSelect">
					
         
		   <a class='dropdown-trigger btn-flat truncate' href='#' data-target='compKPI'><i class="material-icons right">arrow_drop_down</i> Knowage Indicators</a> 
            	<ul id="compKPI"  class='dropdown-content'>



			</ul>
			
          </div>
          <span style="padding-left:20px" id="indicatorValue" class="dropdownValue">none</span>
          </div>
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="idComputed" name="id" type="number" class="validate unalabInput" required>
              <label class="unalabLabel" for="id">Id</label>
            </div>
          </div>
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="nameComputed" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label for="name">Name</label>
            </div>
          </div>
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="descriptionComputed" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label for="description">Description</label>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unitComputed" type="text" name="unit" class="validate unalabInput" required>
              <label for="unit">Unit Measure</label>
            </div>
          </div>     
          <div class="row modal-form-row">  
             
			<div class="input-field col s12" id="compForm">
						
						<input name="city" type="hidden" value="${param.city}"></input> 
						<span>Filter by Indicators level</span> 
						<select
							id="computedLevel" name="level">
							<option value="task">Common Indicators</option>
							<option value="city">City-Level Indicators</option>
							<option value="project">Project-Level Indicators</option>
						</select> 
							
							</div>
							
							<div class="input-field col s12">
							<span>Filter by
							Indicators category</span> 
							<select id="computedCat1" name="cat1">

							<option value="climate">Climate change adaptation and mitigation</option>
							<option value="airquality">Air Quality</option>
							<option value="economic">Economic opportunities and green jobs</option>
							<option value="water">Water management</option>
							<option value="green">Green space management</option>

						</select> 
						<select id="computedCat2" name="cat2">

							<option value="people">People</option>
							<option value="planet">Planet</option>
							<option value="prosperity">Prosperity</option>
							<option value="governance">Governance</option>
							<option value="propagation">Propagation</option>


						</select>
						</div>
						</div>
						
						 <div class="modal-footer">
   
      <button type="submit" id="submitComputed" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Add</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>
						
				</form>
      </div>
      </div>
      
      
    </div>
   
  </div>
  
  
  <!-- Modal Structure -->
  <div id="modal3" class="modal">
    <div class="modal-content">
      <h5 id="titlemodale" class="title"></h5>

	
		
      <div class="row" id="overLapping">
        <form action="/cpm_v2/ModifyCityKPI" method="post">
        <div class="row modal-form-row">
            <div class="input-field col s12">
            
              <input id="tableId" name="tableId" type="number" class="validate unalabInput" disabled>
              <label style="margin-top: -20px" class="unalabLabel active"  for="tableId">Id</label>
            </div>
          </div>
       
       <div class="row modal-form-row"> 
       <div class="input-field col s12">  
          <p >
      		<label class="tooltipped" data-position="bottom" data-tooltip="By checking this, you set the indicator as computed. You will not be able to edit its value">
      			
        			<input id="checkCityComputed" type="checkbox" name="setComp" value="automatic"/>
        			<span>Computed indicator</span>
      		</label>
    		</p>
    		</div>
    	  </div>
    	  
    	  
    	  <div id="shown" class="row modal-form-row hide">
          
          <div class="dropdownSelect">
					
         
		   <a class='dropdown-trigger btn-flat truncate' href='#' data-target='compKPI2'><i class="material-icons right">arrow_drop_down</i> Knowage Indicators</a> 
            	<ul id="compKPI2"  class='dropdown-content'>



			</ul>
			
          </div>
          <span style="padding-left:20px" id="indicatorValue2" class="dropdownValue">none</span>
          </div>
          
          
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea  id="nameEdit" name="name" type="text" class="validate unalabInput materialize-textarea" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="name">Name</label>
            </div>
          </div>
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <textarea id="descEdit" name="desc" type="text" class="materialize-textarea validate unalabInput" required></textarea>
              <label style="margin-top: -20px" class="unalabLabel active" for="description">Description</label>
            </div>
          </div>   
          <div class="row modal-form-row">
            <div class="input-field col s12">
              <input id="unitEdit" type="text" name="unit" class="validate unalabInput" required>
              <label style="margin-top: -20px" class="unalabLabel active" for="unit">Unit Measure</label>
            </div>
          </div> 
          <c:out value="${param['id']}"></c:out>
	    
		
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="id_knowage" />
		
		
		<div class="modal-footer">
   
      <button type="submit" id="submitEdit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Confirm</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>       
       
          </form>
          
          </div>
          </div>
          </div>   
           
  <div class="z-depth-1" style="margin:20px">
  <div id="cont"  style="display:none" >
        <iframe id="cockpit" width="100%" height="600" src="" frameborder="0"></iframe>
      </div>
      
      </div>
      
      
  <!-- Modal Structure -->
  <div id="modal2" class="modal">
    <div class="modal-content">
      <h5 class="title"></h5>

      <div class="row">
        <form action="/cpm_v2/EditKPI" method="post">
          <div class="row modal-form-row" id="modal-body">
          
          <c:out value="${param['id']}"></c:out>
	    <input type="hidden" name="title" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="tableId" />
          </div>
          <div class="row">
            <div class="input-field col s12">
            
              <input class="unalabInput" id="value" name="value" type="number" required>
              <label for="value">Value</label>
            </div>
          </div>
          
          <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Set</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>       
       </form>
      </div>
    </div>
    
  </div>


<!-- Modal Structure -->
  <div id="modalcity" class="modal">
    <div class="modal-content">
      <h5 class="title"></h5>

      <div class="row">
        <form action="/cpm_v2/EditCityKPI" method="post">
          <div class="row modal-form-row" id="modal-body">
          
          <c:out value="${param['id']}"></c:out>
	    <input type="hidden" name="title" />
		<input type="hidden" name="KPI_id" />
		<input type="hidden" name="city" />
		<input type="hidden" name="level" />
		<input type="hidden" name="category" />
		<input type="hidden" name="tableId" />
          </div>
          <div class="row">
            <div class="input-field col s12">
            
              <input class="unalabInput" id="value" name="value" type="number" required>
              <label for="value">Value</label>
            </div>
          </div>
          
          <div class="modal-footer">
   
      <button type="submit" id="submit" name="submit" class=" modal-action modal-close waves-effect waves-green btn-flat" >Set</button>
       <a class=" modal-action modal-close waves-effect waves-red btn-flat" >Cancel</a>
    </div>       
       </form>
      </div>
    </div>
    
  </div>
	
    <script type="text/javascript" src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.98.2/js/materialize.min.js"></script> -->
<script type="text/javascript" src="js/materialize.js"></script>
<script type="text/javascript" src="js/pagination.js"></script>
<script type="text/javascript" src="js/paginathing.js"></script>
<script type="text/javascript" src="js/variable.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script>
<!-- <script type="text/javascript" src="js/jquery.tablesorter.js"></script> -->
<!-- <script type="text/javascript" src="js/jquery.tablesorter.widgets.js"></script> -->
<script type="text/javascript" src="js/custom.js"></script>
<script type="text/javascript" src="js/guest.js"></script>



</body>


</html>