
var prefix = "http://unalab.eng.it:80/knowage/public/servlet/AdapterHTTP?ACTION_NAME=EXECUTE_DOCUMENT_ACTION&OBJECT_LABEL="
var suffix = "&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&SBI_LANGUAGE=en&SBI_COUNTRY=US&NEW_SESSION=true&PARAMETERS="
var suffixnoparameters= "&TOOLBAR_VISIBLE=true&ORGANIZATION=DEFAULT_TENANT&SBI_LANGUAGE=en&SBI_COUNTRY=US&NEW_SESSION=true"

	
//// MANUAL AND CITY MANUAL KPI DASHBOARD ////
var param1 = "kpi="
var param2 = "%26city="
var manualkpidashName = "manualKPInew"
	
//// COMPUTED KPI DASHBOARD ////
var computedName = "ComputedKPI"
var param3 = "%26cityname="
var param4 = "%26unitm="
	
var edit = {
		en: "Edit information for the indicator: ",
		it: "Modifica informazioni per l'indicatore: ",
		fi: "Edit information for the indicator: ",
		du: "Edit information for the indicator: "
};

var wit={
		en: "with ",
		it: "con ",
		fi: "with ",
		du: "with"
};


var noindicators= {
		en: "No indicators to be added for this pilot",
		it: "Nessun indicatore da aggiungere per questo pilota",
		fi: "No indicators to be added for this pilot",
		du: "No indicators to be added for this pilot"
};

var insert = {
		en: "Insert a new value for the indicator: ",
		it: "Inserisci un nuovo valore per l'indicatore: ",
		fi: "Insert a new value for the indicator: ",
		du: "Insert a new value for the indicator: "
};
