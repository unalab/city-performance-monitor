var result;
var cityString;
var idCPM;
var ifSaved = false;

let searchResults=[];

$(document).ready(function() {
	
    drag();
    
    $('.modal').modal();
    $('select').formSelect();
    $('.dropdown-button').dropdown();
    
    var url = new URL(window.location.href);
	console.log("URL: " + url);

	var url_params = new URLSearchParams(url.search);
	city = url_params.get('city');
	idCPM = url_params.get("idCPM");
	console.log("URL CITY: " +city);
	switch(city){
	case "1" :
		cityString = "Genova";
		break;
	case "2":
		cityString ="Tampere";
		break;
	case "3":
		cityString ="Eindhoven";
		break;
	case "Eindhoven":
		cityString ="Eindhoven";
		break;
	case "Genova":
		cityString ="Genova";
		break;
	case "Tempere":
		cityString ="Eindhoven";
		break;
	}
	console.log("CITY STRING " + cityString);
	
	
	$("#titleCity").text("City Performance Monitor - "+ cityString);
	//getMeasures();
	getNewMeasures();
	
	
	$("#measureO").attr("href", "measure.jsp?city="+cityString+"&idCPM="+idCPM);
	$("#formulaO").attr("href", "formula.jsp?city="+cityString+"&idCPM="+idCPM);
	$("#scheduleO").attr("href", "schedule.jsp?city="+cityString+"&idCPM="+idCPM);
	
	
	
});


$("#search2").on("keyup", function() {
	var value = $(this).val();
	
	console.log("VALUE: " + value);
	var set =  $(".collection .collection-item");
	var length = set.length;

	set.each(function(index){
		 console.log(index);
	    if (index != 0 && index < length) {
	    	
	    	$row = $(this);
			
	        var id = $('#'+index).text();
	         console.log(id);
	        if (id.toLowerCase().indexOf(value.toLowerCase()) == -1) {
	        	$('#'+index).hide();
	        } 
	        else if (value!=""){
	        	$('#'+index).show();
	        }
	    } else if (value=="") {
	    	$(".collection .collection-item").show();
	    }
	    
	});
	});

function appendToFormula(clickedId){
	console.log("CLICKED ID: " + clickedId);
	var testo = $('#'+clickedId).text();
	console.log(testo);
	var actualText = $('#formula').val();
	$('#formula').val(actualText+testo);
	
}

function drag(){
	 $(".chip-group .chip span").draggable({
	        helper: "clone"
	    });


 	    $('#textarea').droppable({
	        accept: ".chip-group .chip span",
	        drop: function (event, ui) {
	           var value = $('#formula').val();
//	           
	           if(ui.draggable.text()==="SUM" || ui.draggable.text()==="AVG" || ui.draggable.text()==="MIN" || ui.draggable.text()==="MAX" || ui.draggable.text()==="COUNT"){
	        	   var span = $('<span class="function"></span>').appendTo('#formula');
	        	   span.text(ui.draggable.text());
	        	   $('#formula').val(value + (ui.draggable.text()));
	           }
	           else {// $(this).find('#txtMessageFields').val('');
	           $('#formula').val(value + (ui.draggable.text()));
	           }
	           console.log(ui.draggable.text());
	           $('#formula').focus();
	        }

	    });
	}

function getMeasures(){
	var input = new Object();
	input['city'] = cityString;
	
	$.ajax({
		async: false,
        type: "GET",
        data: input,
        url: "getStatements",
        
        success: function(data){ 
        	console.log(data);
        	json = JSON.parse(data);
        	for (i=0; i<json.length; i++){
        		console.log(json[i]);
        		var empty = json[i].replace("/\s/g", ""); 
        		var chip = $('<div class="chip chip-checkbox" tabindex="0" role="checkbox" aria-checked="false"></div>').appendTo('#chips');
        		chip.attr("aria-labelledby",empty);
        		$('<input type="checkbox" name="checkEx" />').appendTo(chip);
        		var span = $('<span></span>').appendTo(chip);
        		span.attr("id", empty);
        		span.text(json[i]);
        	}
        }
        });
}

function getNewMeasures(){
	var input = new Object();
	input['city'] = cityString;
	
	$.ajax({
		async: false,
        type: "GET",
        data: input,
        url: "getNewStatements",
        
        success: function(data){ 
        	console.log(data);
        	json = JSON.parse(data);
        	searchResults = json;
        	for(i=0; i<json.length; i++){
        		var mes = $('<a href=# class="collection-item"></a>').appendTo('.collectionMeasures');
        		mes.text(json[i].title);
        		mes.attr("id",json[i].id);
        		mes.attr("onclick", "appendToFormula('"+json[i].id+"')");
                
        	}
        	
        }
        });
}

function getFormula(){
	
	
	var formula = $('#formula').val();
	
	var regExp = /\(([^)]+)\)/;
	console.log(formula);
	var arrStr = formula.split(/[+-/*]/);
	try{
	for(var i=0; i<arrStr.length; i++){
	    console.log(arrStr[i]);
	    if(isNaN(arrStr[i])){
	    	var fun = splitFormula(arrStr[i]);
	    	console.log(fun);
	    	
	    		var meas = regExp.exec(arrStr[i]);
	    		console.log(meas[1]);
	    		execMeasure(meas[1],fun);
	    		console.log(result);
	    		formula = formula.replace(arrStr[i],result);
	    		console.log(formula);
	    	
	    }
	}
	console.log(math.evaluate(formula));
	var ris = math.evaluate(formula).toString();
	console.log(ris);
	if(ris)
	$('.ris').text("Result: " + ris);
	result = '';

	}
	catch(err){
		$('.ris').text("ERROR: Check your formula");
	}
	
}


function splitFormula(str) {
	 var i = str.indexOf("(");
	 if(i > 0)
	  return  str.slice(0, i);
	 else
	  return "";     
	}

function execMeasure(misura,funzione){
	var input = new Object();
	
	input['measure'] = misura;
	input['fun'] = funzione; 
	$.ajax({
		async: false,
        type: "GET",
        url: "executeStat",
        data: input,
        success: function(data){
        	console.log(data);
        	result=data;
        }
	
	});
	
}

function saveFormula(){
	
	var formula = $('#formula').val();
	var name = $('#formulaName').val();
	var input = new Object();
	
	input['formula'] = formula;
	input['name'] = name; 
	input['city'] = city;
	input['idCPM'] = idCPM;
	$.ajax({
		async: false,
        type: "POST",
        url: "saveFormula",
        data: input,
        success: function(){
        	
        	alert("Formula saved!");
        }
	
	});
	
	
	
}

//Delay Button display for effect
//document.addEventListener("DOMContentLoaded", function() {
//  let box = document.getElementById("scale");
//  setTimeout(function() {
//    box.classList.add("scale-in");
//  }, 500);
//});

/* hide when expanded*/
//document.querySelector(".search-field").addEventListener("focus", function() {
//  let hidden = document.querySelectorAll(".search-hide");
//
//  for (let i = 0; i < hidden.length; ++i) {
//    hidden[i].style.display = "none";
//  }
//});
//
///* show when expanded*/
//document
//  .querySelector(".search-field")
//  .addEventListener("focusout", function() {
//    let hidden = document.querySelectorAll(".search-hide");
//    for (let i = 0; i < hidden.length; ++i) {
//      hidden[i].style.display = "block";
//    }
//  });
//
//
//
//
//// Event listener for keypress in search box/open
//document.getElementById('search-input').addEventListener('keyup', searchEngine);
//document.getElementById('search-input').addEventListener('focus', searchEngine);
//
//  
//  // Local Search Engine *Basic
//function searchEngine(e){
//  
//  let input = document.getElementById('search-input');
//  console.log("INPUT: " +input);
//  let html = '';
//  let matchingResults = [];
//  let heading = document.querySelector('.search-heading');
//  console.log("searchResults: " + searchResults);
////   Find Matching Results
//  if(input.value === ''){
//    
//    searchResults.forEach(function(obj){
//      heading.textContent = 'Most Recent';
//      
//      console.log("OBJ: " + obj);
//      if(obj.frequent === true){
//        matchingResults.push(obj);
//      }
//    })
//  } else {
//    
//    heading.textContent = 'Search Results';
//    searchResults.forEach(function(obj){
//      if(obj.title.toUpperCase().includes(input.value.toUpperCase())){
//        matchingResults.push(obj);
//      }
//    })
//  }
//  
//
//
//  if(matchingResults.length > 0){
//
//    matchingResults.forEach(function(el){
//      html += `<li><a class="grey-text" href="#" id="${el.link}" onclick="appendToFormula(this.id)">${boldString(el.title, input.value)}</a></li>`
//    })
//    document.querySelector('.popup-list').innerHTML = html;
//  } else{
//    html = `<li>There are no suggestions for your query.</li>`
//    document.querySelector('.popup-list').innerHTML = html;
//  }
//
//}




// Stored search results
//let searchResults = [
//  {
//    title: 'CodePen',
//    description: 'This is just a test',
//    link: '#test1',
//    frequent: false
//  },
//  {
//    title: 'Facebook',
//    description: 'Something else to test',
//    link: '#test2',
//    frequent: false
//  },
//  {
//    title: 'Font Awesome',
//    description: 'Something else to test',
//    link: '#test3',
//    frequent: false
//  },
//  {
//    title: 'Link 1',
//    description: 'Something else to test, just a link name.',
//    link: '#test4',
//    frequent: false
//  },
//  {
//    title: 'Stack Overflow',
//    description: 'Something else to test, just another link name.',
//    link: '#test5',
//    frequent: false
//  },
//  {
//    title: 'Google',
//    description: 'Something else to test, just another link name.',
//    link: '#googletest',
//    frequent: true
//  },
//  {
//    title: 'Apple',
//    description: 'Something else to test, just another link name.',
//    link: '#appletest',
//    frequent: true
//  },
//  {
//    title: 'Microsoft',
//    description: 'Something else to test, just another link name.',
//    link: '#Microsofttest',
//    frequent: true
//  },
//  {
//    title: 'Github',
//    description: 'Something else to test, just another link name.',
//    link: '#githubtest',
//    frequent: true
//  }
//]

// Help bold matching results
//function boldString(str, find){
//  var re = new RegExp(find, 'i');
//  find = re.exec(str);
//  return str.replace(re, '<b>'+find+'</b>');
//}