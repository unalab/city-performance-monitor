

//Init select inputs
$('select').material_select();


//Searcher
$( "#search" ).keyup(function( event ) {
  var value = $(this).val();
  if ( event.which == 13 ) {
     event.preventDefault();
  }
  //console.log("Filter..."+value);
  //renderCards(filterByAttr("title",value, data));
  renderCards(applyFilters());
});

function applyTitleFilter(data){
  var value = $('#search').val();
  return filterByAttr("title",value, data);
}

function filterByAttr(attr, value, data) {
  //console.log(data);
  var value = value.toLowerCase();
  return $.grep(data, function(n, i) {
    return n[attr].toLowerCase().indexOf(value) != -1;

  });
}


function renderCards(data) {
  console.log("RENDERING");
  var html = '';
  $.each(data, function(key, value) {

    html += '<div class="col s12 m6 l4"><div class="card"><div class="card-content white-text"><div class="card__date"><span class="card__date__day">23</span><span class="card__date__month">Mai</span></div><div class="card__meta"><a href="#"><i class="small material-icons">room</i>' + value.city + '</a></div><span class="card-title grey-text text-darken-4">' + value.title + '</span><p class="card-subtitle grey-text text-darken-2">' + value.description + '</p><span class="text-darken-2 card-info"><i class="small material-icons">label</i>&nbsp;' + value.styles + '</span></div><div class="card-action"><a href="#"><i class="material-icons">&nbsp;language</i>VISIT WEB</a><a href="#" class="card-action-right"><i class="material-icons">&nbsp;room</i>FIND</a> </div></div>';

    html += '</div>';
  });
  $('#card-container').html(html);

}

