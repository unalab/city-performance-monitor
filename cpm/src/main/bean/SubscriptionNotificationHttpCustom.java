package main.bean;

import java.util.HashMap;
import java.util.Map;

public class SubscriptionNotificationHttpCustom extends SubscriptionNotificationHttp
{
    private Map<String, String> headers;

    public Map<String, String> getHeaders() {
        return this.headers;
    }

    public void setHeaders(final Map<String, String> headers) {
        this.headers = headers;
    }

    public SubscriptionNotificationHttpCustom(final String url, final Map<String, String> headers) {
        super(url);
        this.headers = headers;
    }

    public SubscriptionNotificationHttpCustom() {
        super("");
        this.headers = new HashMap<String, String>();
    }

    public void putHeader(final String key, final String value) {
        this.headers.put(key, value);
    }
}
