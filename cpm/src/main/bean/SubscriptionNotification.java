package main.bean;

import java.util.HashSet;

public class SubscriptionNotification
{
    int timesSent;
    String lastNotification;
    HashSet<String> attrs;
    String attrsFormat;
    SubscriptionNotificationHttpCustom httpCustom;
    SubscriptionNotificationHttp http;

    public int getTimesSent() {
        return this.timesSent;
    }

    public void setTimesSent(final int timesSent) {
        this.timesSent = timesSent;
    }

    public String getLastNotification() {
        return this.lastNotification;
    }

    public void setLastNotification(final String lastNotification) {
        this.lastNotification = lastNotification;
    }

    public String getAttrsFormat() {
        return this.attrsFormat;
    }

    public void setAttrsFormat(final String attrsFormat) {
        this.attrsFormat = attrsFormat;
    }

    public HashSet<String> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(final HashSet<String> attrs) {
        this.attrs = attrs;
    }

    public SubscriptionNotificationHttpCustom getHttpCustom() {
        return this.httpCustom;
    }

    public void setHttpCustom(final SubscriptionNotificationHttpCustom httpCustom) {
        this.httpCustom = httpCustom;
        this.http = null;
    }

    public SubscriptionNotificationHttp getHttp() {
        return this.http;
    }

    public void setHttp(final SubscriptionNotificationHttp http) {
        this.http = http;
        this.httpCustom = null;
    }

    public SubscriptionNotification(final int timesSent, final String lastNotification, final HashSet<String> attrs, final String attrsFormat, final SubscriptionNotificationHttpCustom httpCustom, final SubscriptionNotificationHttp http) {
        this.timesSent = timesSent;
        this.lastNotification = lastNotification;
        this.attrs = attrs;
        this.attrsFormat = attrsFormat;
        this.httpCustom = httpCustom;
        this.http = http;
    }

    public SubscriptionNotification() {
        this.timesSent = 2;
        this.lastNotification = "";
        this.attrsFormat = "";
        this.attrs = new HashSet<String>();
        this.httpCustom = new SubscriptionNotificationHttpCustom();
        this.http = new SubscriptionNotificationHttp();
    }
}
