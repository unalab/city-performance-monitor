package main.bean;

public class UpdatedEntity
{
    private EntityAttribute<String> value;
    private EntityAttribute<String> lastUpdate;

    public EntityAttribute<String> getValue() {
        return this.value;
    }

    
    public void setLastUpdate(final EntityAttribute<String> lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
    
    public EntityAttribute<String> getLastUpdate() {
        return this.lastUpdate;
    }

    
    public void setValue(final EntityAttribute<String> value) {
        this.value = value;
    }

    public UpdatedEntity() {
        new EntityAttribute(new String());
        new EntityAttribute(new String());
    }

    public UpdatedEntity(final EntityAttribute<String> value, final EntityAttribute<String> lastUpdate) {
        this.value = value;
        this.lastUpdate = lastUpdate;
        
    }
}
