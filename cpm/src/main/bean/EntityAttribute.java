package main.bean;

public class EntityAttribute<T>
{
    private T value;
    private String type;

    public EntityAttribute(final T value) {
        this.setValue(value);
        this.setType(this.value.getClass().getSimpleName());
    }

    public T getValue() {
        return this.value;
    }

    public void setValue(final T value) {
        this.value = value;
    }

    public String getType() {
        return this.type;
    }

    public void setType(final String type) {
        this.type = type;
    }
}
