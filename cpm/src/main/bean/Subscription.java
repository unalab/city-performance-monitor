package main.bean;

public class Subscription
{
    private String id;
    private String description;
    private String status;
    private String expires;
    private int throttling;
    private SubscriptionSubject subject;
    private SubscriptionNotification notification;
    private String attrsFormat;

    public String getId() {
        return this.id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getDescription() {
        return this.description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public String getStatus() {
        return this.status;
    }

    public void setStatus(final String status) {
        this.status = status;
    }

    public String getExpires() {
        return this.expires;
    }

    public void setExpires(final String expires) {
        this.expires = expires;
    }

    public int getThrottling() {
        return this.throttling;
    }

    public void setThrottling(final int throttling) {
        this.throttling = throttling;
    }

    public SubscriptionSubject getSubject() {
        return this.subject;
    }

    public void setSubject(final SubscriptionSubject subject) {
        this.subject = subject;
    }

    public SubscriptionNotification getNotification() {
        return this.notification;
    }

    public void setNotification(final SubscriptionNotification notification) {
        this.notification = notification;
    }

    public String getAttrsFormat() {
        return this.attrsFormat;
    }

    public void setAttrsFormat(final String attrsFormat) {
        this.attrsFormat = attrsFormat;
    }

    public Subscription() {
        this.id = "";
        this.description = "";
        this.status = "";
        this.expires = "";
        this.description = "";
        this.status = "";
        this.expires = "";
        this.subject = new SubscriptionSubject();
        this.notification = new SubscriptionNotification();
    }

    public Subscription(final String id, final String description, final String status, final String expires, final int throttling, final SubscriptionSubject subscriptionSubjectBean, final SubscriptionNotification subscriptionNotificationBean) {
        this.id = "";
        this.description = "";
        this.status = "";
        this.expires = "";
        this.description = description;
        this.status = status;
        this.expires = expires;
        this.throttling = throttling;
        this.subject = subscriptionSubjectBean;
        this.notification = subscriptionNotificationBean;
    }
}
