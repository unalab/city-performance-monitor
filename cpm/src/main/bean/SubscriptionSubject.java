package main.bean;

import java.util.HashSet;
import java.util.Set;

public class SubscriptionSubject
{
    private Set<SubcriptionSubjectEntity> entities;
    private SubscriptionSubjectCondition condition;

    public Set<SubcriptionSubjectEntity> getEntities() {
        return this.entities;
    }

    public void setEntities(final Set<SubcriptionSubjectEntity> entities) {
        this.entities = entities;
    }

    public SubscriptionSubjectCondition getCondition() {
        return this.condition;
    }

    public void setCondition(final SubscriptionSubjectCondition condition) {
        this.condition = condition;
    }

    public SubscriptionSubject(final Set<SubcriptionSubjectEntity> entities, final SubscriptionSubjectCondition condition) {
        this.entities = entities;
        this.condition = condition;
    }

    public SubscriptionSubject() {
        this.entities = new HashSet<SubcriptionSubjectEntity>();
        this.condition = new SubscriptionSubjectCondition();
    }
}
