package main.bean;

import java.util.HashSet;

public class SubscriptionSubjectCondition
{
    private HashSet<String> attrs;
    private SubscriptionSubjectConditionExpression expression;

    public SubscriptionSubjectConditionExpression getExpression() {
        return this.expression;
    }

    public void setExpression(final SubscriptionSubjectConditionExpression expression) {
        this.expression = expression;
    }

    public HashSet<String> getAttrs() {
        return this.attrs;
    }

    public void setAttrs(final HashSet<String> attrs) {
        this.attrs = attrs;
    }

    public SubscriptionSubjectCondition() {
        this.attrs = new HashSet<String>();
        this.expression = null;
        this.attrs = new HashSet<String>();
        this.expression = new SubscriptionSubjectConditionExpression();
    }

    public SubscriptionSubjectCondition(final HashSet<String> attrs, final SubscriptionSubjectConditionExpression expression) {
        this.attrs = new HashSet<String>();
        this.expression = null;
        this.attrs = attrs;
        this.expression = expression;
    }
}
