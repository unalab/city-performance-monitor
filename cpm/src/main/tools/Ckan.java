package main.tools;


import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;


import java.util.ArrayList;

import java.util.HashMap;


import com.google.gson.Gson;
import com.google.gson.JsonParser;
import com.sun.jersey.api.client.ClientResponse;

import main.java.PropertyManager;
import main.java.manualKPIProperties;
import main.utils.RestUtils;

import java.util.Iterator;
import java.util.Map;
import java.time.Instant;
import java.sql.Timestamp;


public class Ckan {
	private static Logger log;
	private static String ckanBaseUrl = PropertyManager.getProperty(manualKPIProperties.CKAN_BASE_URL);
	
	public Ckan(){
		this.log = Logger.getLogger((Class)Ckan.class);
	}
	
	public void ckanDatasetResourceMngt(String message, String city) throws Exception{
		
		
		
		
		JSONObject obj= new JSONObject(message);
		JSONArray data = (JSONArray) obj.get("map");
		
		String refDevice;
		String deviceId;
		String description = "";
		
		refDevice=data.getJSONObject(0).getString("id");
		deviceId = "KPIs";
		
		// Get Organization ID
		System.out.println("Get Organization ID");
		
		// Get Organization Name
		
		String orgName = "unalab";
		System.out.println("organization name:" + orgName);
		// Define CKAN dataset
		String dataset=orgName+"_"+city.toLowerCase()+"_allkpis";	
		System.out.println("dataset da creare:" + dataset + "\n");
		// Get CKAN Org Id
		String ckanOrgId=getCkanOrgId(orgName);
		if(ckanOrgId!=""){
			System.out.println("CKAN Organization Id <"+ckanOrgId+">");
			}
			else{
			    //ckanOrgId = "7c0ca7a9137e448a98ed863069e41043";
			    ckanOrgId = "unalab";
				
			}
		
		// Check if Dataset exists
		Boolean datasetExists=checkCkanDataset(dataset);

				
		if (!datasetExists){
			// Create Dataset
			System.out.println("<"+dataset+"> Create");
			createCkanDataset(ckanOrgId,dataset,city);
			
			
		}
		else {
			deleteCkanResource(dataset);
			createCkanDataset(ckanOrgId,dataset,city);
		}
		
		
		String resourceId="";
		
		resourceId=getCkanResourceId(dataset, deviceId);
		// If Resource not exists, Create resource and datastore otherwise Update
		
			
			
		
			if (resourceId!=""){ 
				
				System.out.println("<"+deviceId+"> Update");
				
			}
			else{
				System.out.println("<"+deviceId+"> Create");
				System.out.println("Datastore Create");
				
				
				createCkanDatastore2(dataset, data, deviceId, description);
				
			
		}		
		
		resourceId=getCkanResourceId(dataset, deviceId);
			
		System.out.println("Datastore Update");
		
		upsertCkanDatastore2(resourceId, data);
		
	}
	
	
	
	private static Boolean checkCkanDataset(String dataset) throws Exception{
		String ckanEndpoint = ckanBaseUrl + "package_show?id="+dataset;
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
		ClientResponse resp = RestUtils.consumeGet2(ckanEndpoint,headers);
		
		if (resp.getStatus()==200){
			System.out.println("<" + dataset + "> Dataset found");
			return true;
		}
		else if (resp.getStatus()==404){
			System.out.println("<" + dataset + "> Dataset not found");
			return false;
		}
		else {
			throw new Exception(String.valueOf(resp.getStatus()));
		}
	}
	
	private static String getCkanResourceId(String dataset, String resname) throws Exception{
		String ckanEndpoint = ckanBaseUrl + "package_show?id="+dataset;
		String resId="";
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
		ClientResponse resp = RestUtils.consumeGet2(ckanEndpoint,headers);
		
		if (resp.getStatus()==200){
			String serviceResponse = RestUtils.getBody(resp);
			JSONObject obj= new JSONObject(serviceResponse);
			JSONArray resources = (JSONArray) obj.getJSONObject("result").get("resources");
			Integer numResources=0;
			for(int i=0; i<resources.length(); i++){
				
				numResources++;
				if (resources.getJSONObject(i).getString("name").equals(resname)){
					resId=resources.getJSONObject(i).getString("id");
				}
			}
			return resId;
		}
		else {
			throw new Exception(String.valueOf(resp.getStatus()));
		}
		
		
	}
	
	
	
	private static void createCkanDataset(String ckanOrgId, String dataset, String city) throws Exception{
		String ckanEndpoint = ckanBaseUrl + "package_create";
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
		String body="{"+
						"\"name\":\""+dataset+"\","+
						"\"title\":\""+dataset+"\","+
						"\"private\":false,"+
						"\"author\":\""+PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY)+"\","+
						"\"maintainer\":\""+PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY)+"\","+
						"\"state\":\"active\","+
						"\"notes\":\"Dataset automatically created\","+
						"\"groups\":"+
							"["+
								"{"+  
								   "\"name\":\""+city+"\""+
								"}"+
							"],"+
						 "\"owner_org\":\""+ckanOrgId+"\""+
						"}";
		ClientResponse resp = RestUtils.consumePost2(ckanEndpoint,body,headers);
		// If the response Status is not OK throws a new exception
		checkStatus(resp);
		
		System.out.println("Dataset <" + dataset + "> created");
	}
	
	private static Boolean checkStatus(ClientResponse resp) throws Exception{
		if(resp.getStatus()!=200 && resp.getStatus()!=201 && resp.getStatus()!=204 && resp.getStatus()!=301){
			String serviceResponse = RestUtils.getBody(resp);
			throw new Exception(String.valueOf(resp.getStatus()) + " " + serviceResponse);
		}
		
		return true;
	}
	
	
	
	
	private static void deleteCkanResource(String resid) throws Exception{
		String ckanEndpoint = ckanBaseUrl + "dataset_purge";
		String body="{"+
						"\"id\":\""+resid+"\""+
						
					"}";
		System.out.println(body);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
		
		ClientResponse resp = RestUtils.consumePost2(ckanEndpoint, body, headers);
		//If the response Status is not OK throws a new exception
		checkStatus(resp);
		System.out.println("Datastore deleted");
	}
	
	
	
	
	private static void createCkanDatastore2(String resid, JSONArray data, String resname, String description) throws Exception{
		String ckanEndpoint = ckanBaseUrl + "datastore_create";
		String body="{"+
						"\"resource\":"+
						"{"+
							"\"name\":\""+resname+"\","+
							"\"package_id\":\""+resid+"\","+
							"\"format\":\"json\","+
							"\"state\":\"active\","+
							"\"description\":\""+description+"\""+
				        "},"+
						"\"fields\":["+
						"{\"id\":\"recvTime\","+
			            "\"type\":\"timestamp\"}";
						//"{\"id\":\"recvTimeTs\","+
				        //"\"type\":\"int\"}";
		
		
		body=body+
				",{"+
				"\"id\":\"Id\","+
				"\"type\":\"text\""+
				"}"+
				",{"+
				"\"id\":\"Name\","+
				"\"type\":\"text\""+
				"}"+
				",{"+
				"\"id\":\"Description\","+
				"\"type\":\"text\""+
				"}"+
				",{"+
				"\"id\":\"Value\","+
				"\"type\":\"text\""+
				"}"+
				",{"+
				"\"id\":\"Last Update\","+
				"\"type\":\"text\""+
				"}";
		
//		for (int i = 0; i < data.length(); i++) {
//			String id =(String) data.getJSONObject(i).get("id");
//			String name = (String) data.getJSONObject(i).getJSONObject("name").get("value");
//			System.out.println("ID: "+ id + ", NAME: " + name);
//		}
		
		
//		Iterator<?> keys = data.getJSONObject(0).keys();
//		while(keys.hasNext() ) {
//			String key = (String)keys.next();
//			System.out.println(key);
//			if(key.equalsIgnoreCase("tableId")) {
//        		
//        	    
//        	    key = "Indicator ID";
//        	    	
//        }
//        else if (key.equalsIgnoreCase("isManual")) {
//        		
//        		key="Type";
//        }
//			
//        else if (key.equalsIgnoreCase("date")){
//        	
//        	key = "Last Modified";
//        }
//        else if (key.equalsIgnoreCase("motivation")){
//        	
//        	key = "Value Motivation";
//        }
//        else if (key.equalsIgnoreCase("description")){
//        	
//        	key = "Description";
//        }
//        else if (key.equalsIgnoreCase("unit")){
//        	
//        	key = "Measure Unit";
//        }
//        else if (key.equalsIgnoreCase("name")){
//        	
//        	key = "Name";
//        }
//        else if (key.equalsIgnoreCase("value")){
//        	
//        	key = "Value";
//        }
//        
//			
//			if ((!key.equals("idKPI")) && (!key.equals("idCity")) && (!key.equals("kpiLevel")) && (!key.equals("kpiCategory")) && (!key.equals("icon"))&& (!key.equals("idKnowage"))) {
//				body=body+
//					",{"+
//					"\"id\":\""+key+"\","+
//					"\"type\":\"text\""+
//					"}";
//			}
//		}
		body=body+
				"],"+
				"\"force\":\"true\""+
				"}";
		System.out.println(body);
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
	
		ClientResponse resp = RestUtils.consumePost2(ckanEndpoint,body,headers);
		//If the response Status is not OK throws a new exception
		checkStatus(resp);
		System.out.println("Datastore created");
	}
	
	
	
	private static void upsertCkanDatastore2(String resid, JSONArray data) throws Exception{

		System.out.println("Data in upsert: " + data);
		
		Timestamp timestamp = new Timestamp(System.currentTimeMillis());
		Instant instant = timestamp.toInstant();
		String ckanEndpoint = ckanBaseUrl + "datastore_upsert";
		
		String body="{"+
						"\"resource_id\":\""+resid+"\","+
						"\"records\":["; 
		for (int i = 0; i < data.length(); i++) {
			if(i!=0) {
	    		body = body + ",{";
			}
			else {
	    		body = body + "{";
	    		
			}
			body = body +"\"recvTime\":\""+instant+"\"";
			String id =(String) data.getJSONObject(i).get("id");
			body = body +",\"Id\":"+ "\""+id+"\"";
			String name = (String) data.getJSONObject(i).getJSONObject("name").get("value");
			body = body +",\"Name\":"+ "\""+name+"\"";
			String description = (String) data.getJSONObject(i).getJSONObject("description").get("value");
			body = body +",\"Description\":"+ "\""+description+"\"";
			Integer value= (Integer) data.getJSONObject(i).getJSONObject("kpiValue").get("value");
			body = body +",\"Value\":"+ "\""+value+"\"";
			String date = (String) data.getJSONObject(i).getJSONObject("dateModified").get("value");
			body = body +",\"Last Update\":"+ "\""+date+"\"";
			//System.out.println("ID: "+ id + ", NAME: " + name);
		    body = body + "}";
			
		}	
		     body=body+"],"+"\"method\":\"insert\","+"\"force\":\"true\""+"}";
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
		System.out.println("body: " + body);
		ClientResponse resp = RestUtils.consumePost2(ckanEndpoint,body,headers);
		// If the response Status is not OK throws a new exception
		checkStatus(resp);
		System.out.println("Datastore updated");
		
	}
	
	
	public static String getCkanOrgId(String orgName) throws Exception{
		String orgId="";
		String ckanEndpoint = ckanBaseUrl + "organization_show?id="+orgName.toLowerCase();
		
		Map<String, String> headers = new HashMap<String, String>();
		headers.put("Authorization", PropertyManager.getProperty(manualKPIProperties.CKAN_DEFAULT_USER_APIKEY));
		ClientResponse resp = RestUtils.consumeGet2(ckanEndpoint, headers);
		
		if (resp.getStatus()==200){
			String serviceResponse = RestUtils.getBody(resp);
			JSONObject obj= new JSONObject(serviceResponse);
			orgId=obj.getJSONObject("result").getString("id");
			System.out.println("<" + orgName + "> Organization found with CKAN ID <" + orgId + ">");
			return orgId;
		}
		else if (resp.getStatus()==404){
			System.out.println("<" + orgName + "> Organization not found");
			return orgId;
		}
		else {
			throw new Exception(String.valueOf(resp.getStatus()));
		}
	}
	
	
}