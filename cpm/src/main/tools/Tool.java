package main.tools;

import java.net.MalformedURLException;
import main.utils.CommonUtils;
import com.google.gson.Gson;

public abstract class Tool
{
    private String baseUrl;
    private Gson gson;

    protected Tool(final String baseurl) throws Exception {
        this.gson = new Gson();
        this.setBaseUrl(baseurl);
    }

    public String getBaseUrl() {
        return this.baseUrl;
    }

    private void setBaseUrl(final String url) throws Exception {
        if (url == null) {
            throw new NullPointerException("ContextBroker URL is NULL");
        }
        final String s = url.trim();
        if (!CommonUtils.isValidURL(s)) {
            throw new MalformedURLException("ContextBroker URL is not valid");
        }
        this.baseUrl = url;
    }

    protected Gson getGson() {
        return this.gson;
    }

    protected void setGson(final Gson gson) {
        this.gson = gson;
    }
}
