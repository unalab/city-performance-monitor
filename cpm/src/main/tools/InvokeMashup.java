//package main.tools;
//
//import javax.servlet.ServletContextEvent;
//import java.io.IOException;
//import main.utils.RestUtils;
//import java.util.Properties;
//import org.json.JSONException;
//import java.util.ArrayList;
//import org.json.JSONObject;
//import main.java.KPIBean;
//import main.java.Queries;
//import org.json.JSONArray;
//import main.java.PropertyManager;
//import main.java.manualKPIProperties;
//import java.util.logging.Logger;
//import javax.servlet.annotation.WebListener;
//import javax.servlet.ServletContextListener;
//
//@WebListener
//public class InvokeMashup extends Dme implements ServletContextListener
//{
//    Logger logger;
//
//    public InvokeMashup() throws Exception {
//        super(String.valueOf(PropertyManager.getProperty(manualKPIProperties.DmeProtocol)) + PropertyManager.getProperty(manualKPIProperties.DmeHost));
//        this.logger = Logger.getLogger(InvokeMashup.class.getName());
//    }
//
//    public static JSONArray buildInput(final Integer id_city) throws JSONException {
//        JSONObject kpi = null;
//        final JSONArray json = new JSONArray();
//        final ArrayList<KPIBean> kpisManual = Queries.getDownloadKPI(id_city, "manual");
//        final ArrayList<KPIBean> kpisCity = Queries.getDownloadKPI(id_city, "citymanual");
//        ArrayList<KPIBean> kpis  = new ArrayList<KPIBean>();
//        if(kpisManual!=null)
//        kpis.addAll(kpisManual);
//        if(kpisCity!=null)
//        kpis.addAll(kpisCity);
//        String cityS = Queries.getCityName(id_city);
//        //Integer category_city = Queries.getCategoryId(cityS.toUpperCase(), "KPI_KPI_CATEGORY");
//        //ArrayList<KPIBean> intermediate = (ArrayList<KPIBean>)Queries.getDownloadComputedKPI(id_city);
//        
//		//ArrayList<Integer> ids = (ArrayList<Integer>)Queries.getIds((ArrayList)intermediate, category_city);
//        //ArrayList<KPIBean> kpicomputedList = (ArrayList<KPIBean>)Queries.getComputedValues((ArrayList)intermediate, (ArrayList)ids);
////        if(kpicomputedList!=null) {
////        	kpis.addAll(kpicomputedList);
////        }
////        
//        final JSONArray category = new JSONArray();
//        category.put((Object)"quantitative");
//        category.put((Object)"qualitative");
//        float value = 0.0f;
//        Integer valueComputed = 0;
//        final String city = Queries.getCityName(id_city);
//        boolean isPresent4City = false;
//        for (int i = 0; i < kpis.size(); ++i) {
//            if (kpis.get(i).getIsManual().equalsIgnoreCase("manual") || kpis.get(i).getIsManual().equalsIgnoreCase("citymanual")) {
//                //value = Queries.getManualValue(kpis.get(i).getIdKPI(), id_city);
//                kpi = new JSONObject();
//                kpi.put("id", (Object)kpis.get(i).getIdKPI());
//                kpi.put("name", (Object)kpis.get(i).getName());
//                kpi.put("description", (Object)kpis.get(i).getDescription());
//                kpi.put("source", (Object)"UNaLab");
//                kpi.put("organization", (Object)"UNaLab");
//                kpi.put("calculationFrequency", (Object)"daily");
//                kpi.put("category", (Object)category);
//                kpi.put("value", kpis.get(i).getValue());
//                json.put((Object)kpi);
//            }
////            else {
//////                final Integer categoryCity = Queries.getCategoryId(city, "KPI_KPI_CATEGORY");
//////                final Integer idKnowage = Queries.getIdKnowageByidKpi(kpis.get(i).getIdKPI());
////                isPresent4City = Queries.isPresent4City(idKnowage, categoryCity);
////                if (isPresent4City) {
////                    valueComputed = Queries.getLastValue(idKnowage);
////                    value = valueComputed;
////                    kpi = new JSONObject();
////                    kpi.put("id", (Object)kpis.get(i).getIdKPI());
////                    kpi.put("name", (Object)kpis.get(i).getName());
////                    kpi.put("description", (Object)kpis.get(i).getDescription());
////                    kpi.put("source", (Object)"UNaLab");
////                    kpi.put("organization", (Object)"31a56233270c46eb8585e2ee41411c89");
////                    kpi.put("calculationFrequency", (Object)"daily");
////                    kpi.put("category", (Object)category);
////                    kpi.put("value", (double)value);
////                    json.put((Object)kpi);
////                }
////            }
//        }
//        return json;
//    }
//
//    public boolean invokeDme(final Integer id_city) throws JSONException, IOException {
//        final String city = Queries.getCityName(id_city);
//        System.out.println("CITY NAME: " + city);
//        final Properties props = new Properties();
//        props.load(InvokeMashup.class.getClassLoader().getResourceAsStream("config.properties"));
//        final String propertyname = "dme.invoke.mashup." + city.toLowerCase();
//        System.out.println("PROPERTY NAME: " + propertyname);
//        final String citypath = props.getProperty(propertyname);
//        System.out.println("CITY PROPERTY: " + citypath);
//        final String url = String.valueOf(this.getBaseUrl()) + citypath;
//        final JSONArray input = buildInput(id_city);
//        
//       String jsonToString = input.toString();
//       jsonToString = jsonToString.replace("%", "percentage");
//        
//        System.out.println("INPUT: " + jsonToString);
//        boolean out = true;
//        try {
//            RestUtils.consumePost(url, (Object)jsonToString);
//            
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            out = false;
//        }
//        return out;
//    }
//    
//    public String invokeDme2(final Integer id_city) throws JSONException, IOException {
//        final String city = Queries.getCityName(id_city);
//        System.out.println("CITY NAME: " + city);
//        String json = null;
//        final Properties props = new Properties();
//        props.load(InvokeMashup.class.getClassLoader().getResourceAsStream("config.properties"));
//        final String propertyname = "dme.noorion.invoke." + city.toLowerCase();
//        System.out.println("PROPERTY NAME: " + propertyname);
//        final String citypath = props.getProperty(propertyname);
//        System.out.println("CITY PROPERTY: " + citypath);
//        final String url = String.valueOf(this.getBaseUrl()) + citypath;
//        final JSONArray input = buildInput(id_city);
//        System.out.println("INPUT: " + input.toString());
//        boolean out = true;
//        try {
//           json = RestUtils.consumePost(url, (Object)input.toString());
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//            out = false;
//        }
//        return json;
//    }
//
//    public void contextDestroyed(final ServletContextEvent arg0) {
//    }
//
//    public void contextInitialized(final ServletContextEvent arg0) {
//    }
//}
