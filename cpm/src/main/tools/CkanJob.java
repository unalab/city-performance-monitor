//package main.tools;
//
//import org.quartz.JobExecutionException;
//import java.util.Iterator;
//import java.util.ArrayList;
//import main.bean.City;
//import main.java.Queries;
//import org.quartz.JobExecutionContext;
//import org.apache.log4j.Logger;
//import org.quartz.Job;
//
//public class CkanJob implements Job
//{
//    private Logger log;
//
//    public CkanJob() {
//        this.log = Logger.getLogger((Class)CkanJob.class);
//    }
//
//    public void execute(final JobExecutionContext jExeCtx) throws JobExecutionException {
//        this.log.debug((Object)"Job Ckan run successfully...");
//        final ArrayList<City> cities = Queries.getCities();
//        try {
//            final GenerateData data = new GenerateData();
//            for (final City c : cities) {
//                this.log.debug((Object)("CITY: " + c.getName()));
//                data.invokeCkan(c.getName());
//            }
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//}
