package main.java;

import java.io.IOException;
import javax.servlet.ServletException;


import org.json.JSONException;
import org.json.JSONObject;
import main.utils.SubscriptionManager;
import java.util.HashSet;
import main.bean.City;
import main.kpiEngine.service.CalculateKPI;


import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/AddKPI" })
public class AddKPI extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public AddKPI() {
        this.logger = Logger.getLogger(AddKPI.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
       
        final float value = 0.0f;
        String idP = request.getParameter("id");
        String name = request.getParameter("name");
        name =name.trim();
        this.logger.debug((Object)("name: " + name));
        String desc = request.getParameter("desc");
        desc=desc.trim();
        this.logger.debug((Object)("description: " + desc));
        String unit = request.getParameter("unit");
        unit = unit.trim();
        this.logger.debug((Object)("unit measure: " + unit));
        String cityP = request.getParameter("city");
        this.logger.debug((Object)("city: " + cityP));
        String level = request.getParameter("level");
        String nbs = request.getParameter("nbs");
        this.logger.debug((Object)("level: " + level));
        Integer city = Integer.parseInt(cityP);
        Integer id = Integer.parseInt(idP);
        String valueS = "0";
        String kpiName="";
        String type = request.getParameter("indType");
        
        if(type.equalsIgnoreCase("computed")) {
        	kpiName = request.getParameter("kpiName");
        	
        }
        ArrayList<City> cities = new ArrayList<City>();
        
        Integer idKPI = 0;
        
        String category;
        if (level.equalsIgnoreCase("task")) {
            category = request.getParameter("cat1");
        }
        else {
            category = request.getParameter("cat2");
        }
        
        String motivation = "default value";
        this.logger.debug((Object)("category: " + category));
        idKPI = Queries.addManualKPI(motivation,id, name, desc, unit, city, level, category, type, value,nbs);
        String idKpi = idKPI.toString();
        String cityname = Queries.getCityName(city);
        String db = cityname.toLowerCase()+"_ManualKPI"+idKpi;
        CalculateKPI.saveAsMeasure(db, name, cityname.toLowerCase());
        String createdOnUpdateKPISubscription = null;
        String createdKPISubscriptionEntity = null;
        String subscriptionIdCreated = null;
        JSONObject subscrIdj = null;
        String fiware_service = cityname.toLowerCase();
        String fiware_servicepath = "/"+cityname.toLowerCase()+"_kpi/harmonized";
        final HashSet<String> notificationAttrs = new HashSet<String>();
        notificationAttrs.add("value");
       // logger.debug(idP+ ", " + name + ", " + notificationAttrs + ", "+ fiware_service + ", "+ fiware_servicepath);
        try {
			createdOnUpdateKPISubscription = SubscriptionManager.createDeviceSubscription(idKpi.toString()+"_ManualKPI", name, (HashSet)notificationAttrs, fiware_service, fiware_servicepath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			subscrIdj = new JSONObject(createdOnUpdateKPISubscription);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			subscriptionIdCreated = subscrIdj.getString("subscriptionId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        try {
			createdKPISubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(name, "number", id.toString(),idKpi.toString()+"_manual_v2", subscriptionIdCreated, "0", fiware_service, fiware_servicepath);
			Queries.updateEntityKpi(Integer.parseInt(idKpi),idKpi+"_manual_v2");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        final String nextJSP = "/cpm_v2/view.jsp?city=" + city + "&level=" + level + "&category=" + category+"&lang=en";
        response.sendRedirect(nextJSP);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
