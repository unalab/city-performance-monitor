package main.java;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Connection;
import org.apache.log4j.Logger;

public class DBConnectionEngine
{
    static Logger logger;

    static {
        DBConnectionEngine.logger = Logger.getLogger(DBConnectionEngine.class.getName());
    }

    public static Connection createConnection() {
        Connection con = null;
        final String url = PropertyManager.getProperty(manualKPIProperties.DB_KPIENGINE_HOST);
        final String username = PropertyManager.getProperty(manualKPIProperties.DB_USERNAME);
        final String password = PropertyManager.getProperty(manualKPIProperties.DB_PASSWORD);
        System.out.println("URL: " + url + " username: "+ username + " password: " + password);
        try {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            }
            catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
            con = DriverManager.getConnection(url, username, password);
            
           
            
        }
        catch (Exception e2) {
            e2.printStackTrace();
        }
        return con;
    }
}
