package main.java;

public class KPIBean
{
    private Integer idKPI;
    private Integer idCity;
    private String name;
    private String description;
    private float value;
    private String unit;
    private String date;
    private String kpiLevel;
    private String kpiCategory;
    private Integer tableId;
    private String humanCategory;
    private String icon;
    private Integer idKnowage;
    private String isManual;
    private Integer knowageCity;
    private String motivation;
    private String nbs;
    private String entity;

    public void setEntity(String entity) {
    	this.entity = entity;
    }
    
    public String getEntity() {
    	return this.entity;
    }
    
    public void setNbs (final String nbs) {
    	this.nbs = nbs;
    }
    
    public String getNbs () {
    	return this.nbs;
    }
    public void setKnowageCity(final Integer knowageCity) {
        this.knowageCity = knowageCity;
    }

    public Integer getKnowageCity() {
        return this.knowageCity;
    }

    public void setIdKnowage(final Integer idKnowage) {
        this.idKnowage = idKnowage;
    }

    public Integer getIdKnowage() {
        return this.idKnowage;
    }

    public void setIcon(final String icon) {
        this.icon = icon;
    }

    public String getIcon() {
        return this.icon;
    }

    public void setIdKPI(final Integer idKPI) {
        this.idKPI = idKPI;
    }

    public void setIdCity(final Integer idCity) {
        this.idCity = idCity;
    }

    public void setTableId(final Integer tableId) {
        this.tableId = tableId;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public void setMotivation(final String motivation) {
        this.motivation = motivation;
    }

    public void setValue(final float value) {
        this.value = value;
    }

    public void setUnit(final String unit) {
        this.unit = unit;
    }

    public void setDate(final String date) {
        this.date = date;
    }

    public void setKpiLevel(final String kpiLevel) {
        this.kpiLevel = kpiLevel;
    }

    public void setKpiCategory(final String kpiCategory) {
        this.kpiCategory = kpiCategory;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public String getMotivation() {
        return this.motivation;
    }

    public float getValue() {
        return this.value;
    }

    public String getUnit() {
        return this.unit;
    }

    public String getDate() {
        return this.date;
    }

    public String getKpiLevel() {
        return this.kpiLevel;
    }

    public String getKpiCategory() {
        return this.kpiCategory;
    }

    public Integer getIdKPI() {
        return this.idKPI;
    }

    public Integer getIdCity() {
        return this.idCity;
    }

    public Integer getTableId() {
        return this.tableId;
    }

    public String getHumanCategory() {
        return this.humanCategory;
    }

    public void setHumanCategory(final String humanCategory) {
        this.humanCategory = humanCategory;
    }

    public void setIsManual(final String isManual) {
        this.isManual = isManual;
    }

    public String getIsManual() {
        return this.isManual;
    }
}
