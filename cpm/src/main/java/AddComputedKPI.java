//package main.java;
//
//import java.io.IOException;
//import javax.servlet.ServletException;
//
//import org.json.JSONObject;
//import main.utils.SubscriptionManager;
//import java.util.HashSet;
//import main.bean.City;
//import main.tools.GenerateData;
//
//
//import java.util.ArrayList;
//import javax.servlet.http.HttpServletResponse;
//import javax.servlet.http.HttpServletRequest;
//import org.apache.log4j.Logger;
//import javax.servlet.annotation.WebServlet;
//import javax.servlet.http.HttpServlet;
//
//@WebServlet({ "/AddComputedKPI" })
//public class AddComputedKPI extends HttpServlet
//{
//    private static final long serialVersionUID = 1L;
//    Logger logger;
//
//    public AddComputedKPI() {
//        this.logger = Logger.getLogger(AddComputedKPI.class.getName());
//    }
//
//    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
//        final String isManual = "automatic";
//        String fiware_service = null;
//        String fiware_servicepath = null;
//        String createdOnUpdateKPISubscription = null;
//        String createdKPISubscriptionEntity = null;
//        String subscriptionIdCreated = null;
//        JSONObject subscrIdj = null;
//        final String idtable = request.getParameter("id");
//        this.logger.debug((Object)("ID: " + idtable));
//        final String id_knowage = request.getParameter("id_knowage");
//        final String name = request.getParameter("name");
//        this.logger.debug((Object)("name: " + name));
//        final String desc = request.getParameter("desc");
//        this.logger.debug((Object)("description: " + desc));
//        final String unit = request.getParameter("unit");
//        this.logger.debug((Object)("unit measure: " + unit));
//        String nbs = request.getParameter("nbs");
//        final String cityP = request.getParameter("city");
//        this.logger.debug((Object)("city: " + cityP));
//        final String level = request.getParameter("level");
//        this.logger.debug((Object)("level: " + level));
//        final Integer city = Integer.parseInt(cityP);
//        final Integer numberId = Integer.parseInt(id_knowage);
//        final Integer lastValue = Queries.getLastValue(numberId);
//        final String valueS = lastValue.toString();
//        ArrayList<City> cities = new ArrayList<City>();
//        final HashSet<String> notificationAttrs = new HashSet<String>();
//        notificationAttrs.add("value");
//        
//        String category;
//        if (level.equalsIgnoreCase("task")) {
//            category = request.getParameter("cat1");
//        }
//        else {
//            category = request.getParameter("cat2");
//        }
//        this.logger.debug((Object)("category: " + category));
//        final Integer id = Integer.parseInt(idtable);
//        Integer idKPI = Queries.addKPI(id, name, desc, unit, city, level, category, isManual, 0.0f, nbs);
//        String idKpi = idKPI.toString();
////        InvokeMashup mashup = null;
////        try {
////			 mashup = new InvokeMashup();
////		} catch (Exception e1) {
////			// TODO Auto-generated catch block
////			e1.printStackTrace();
////		}
//        GenerateData ckan = new GenerateData();
//        String cityname = Queries.getCityName(city);
//        try {
//            //cities = (ArrayList<City>)Queries.getCities();
//            //for (final City c : cities) {
//                fiware_service = cityname.toLowerCase();
//                fiware_servicepath = "/" + cityname.toLowerCase() + "_computedKPI";
//                createdOnUpdateKPISubscription = SubscriptionManager.createDeviceSubscription(String.valueOf(idKPI), name, (HashSet)notificationAttrs, fiware_service, fiware_servicepath);
//                subscrIdj = new JSONObject(createdOnUpdateKPISubscription);
//                subscriptionIdCreated = subscrIdj.getString("subscriptionId");
//                this.logger.info((Object)("Subscription Id Created: " + subscriptionIdCreated));
//                createdKPISubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(name,unit,idtable, String.valueOf(idKPI), subscriptionIdCreated, valueS, fiware_service, fiware_servicepath);
//                this.logger.info((Object)("Created KPI Subscription Entity on Orion: " + createdKPISubscriptionEntity));
//                //mashup.invokeDme(city);
//                ckan.invokeCkan(cityname);
//                
//            //}
//        }
//        catch (Exception e) {
//            e.printStackTrace();
//        }
//        final String nextJSP = "/cpm_v2/view.jsp?city=" + city + "&level=" + level + "&category=" + category;
//        response.sendRedirect(nextJSP);
//    }
//
//    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
//        this.doGet(request, response);
//    }
//}
