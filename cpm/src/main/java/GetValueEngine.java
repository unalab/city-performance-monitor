package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/GetValueEngine" })
public class GetValueEngine extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public GetValueEngine() {
        this.logger = Logger.getLogger(GetValueEngine.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        System.out.println("ID: " + id);
        String name = Queries.getValueEngine(id);
        System.out.println("NAME: " +name);
        response.getWriter().write(name);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
