package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/LogoutServlet" })
public class LogoutServlet extends HttpServlet
{
    private static final long serialVersionUID = 1L;

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final HttpSession session = request.getSession(false);
        if (session != null) {
            session.invalidate();
            request.setAttribute("errMessage", (Object)"You have logged out successfully");
            response.sendRedirect(String.valueOf(request.getContextPath()) + "/index.jsp");
        }
        System.out.println("Logged out");
    }
}
