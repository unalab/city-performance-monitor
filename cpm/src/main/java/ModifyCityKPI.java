package main.java;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/ModifyCityKPI" })
public class ModifyCityKPI extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public ModifyCityKPI() {
        this.logger = Logger.getLogger(ModifyCityKPI.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String isManual = request.getParameter("setComp");
        String desc = request.getParameter("desc");
        String unit = request.getParameter("unit");
        String idCityP = request.getParameter("city");
        String level = request.getParameter("level");
        String category = request.getParameter("category");
        String KPI_idP = request.getParameter("KPI_id");
        //final String id_knowage = request.getParameter("id_knowage");
        String nbs = request.getParameter("nbs");
        String kpiName = request.getParameter("kpiName");
        String auto = request.getParameter("auto2");
        System.out.println(auto);
    	//System.out.println(kpiName);
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        Integer idCity;
        if (idCityP.matches(".*\\d.*")) {
            idCity = Integer.parseInt(idCityP);
        }
        else {
            idCity = Queries.getCityId(idCityP);
        }
        final Integer KPI_id = Integer.parseInt(KPI_idP);
        System.out.println("IS MANUAL: " + isManual);
        if (isManual != null) {
        	
        	
            isManual = "computed";
            Queries.updateKPI(KPI_id, name, desc, unit, isManual,idCity, nbs);
            Integer checked = Queries.checkInfluxData(KPI_id);
            if(checked!=0) {
            	Queries.deleteId(KPI_id);
            }
            Queries.updateFormula(auto,KPI_id);
            
            String entityId = Queries.getOrionEntity(auto);
            
            Queries.updateEntityKpi(KPI_id,entityId);
            
          
        }
        else {
            isManual = "citymanual";
            //final boolean presente = Queries.isPresentManualKpi(KPI_id);
            //if (!presente) {
                Queries.updateKPI(KPI_id, name, desc, unit, isManual,idCity, nbs);
              //  Queries.editKPIValue(idCity, KPI_id, 0.0f, "default value");
            //}
//            else {
//                Queries.updateKPI(KPI_id, name, desc, unit, isManual, idCity, nbs);
//            }
        }
        final String nextJSP = "/cpm_v2/view.jsp?city=" + idCity + "&level=" + level + "&category=" + category + "&lang=" + lang;
        response.sendRedirect(nextJSP);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
