package main.java;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.gson.Gson;

import main.kpiEngine.service.Queries;

@WebServlet("/getCpmKPIs")
public class getCpmKPIs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public getCpmKPIs() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String city = request.getParameter("city");
		JSONArray KPIs = new JSONArray();
		try {
			KPIs = Queries.getCpmKPIs(city);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		String jsonString = gson.toJson(KPIs);
		
		jsonString = jsonString.replace("{\"myArrayList\":[{", "[");
		jsonString = jsonString.replace("\"map\":{", "{");
		jsonString = jsonString.replace("},{", ",");
		jsonString = jsonString.replace ("}}]}", "}]");
		System.out.println("ECCO IL JSON:" + jsonString);
		response.getWriter().write(jsonString);
	}

}
