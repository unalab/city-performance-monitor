package main.java;

import java.io.IOException;
import javax.servlet.ServletException;



import org.json.JSONObject;
import main.utils.SubscriptionManager;
import java.util.HashSet;
import main.bean.City;
import main.kpiEngine.service.CalculateKPI;

import java.util.ArrayList;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletRequest;
import org.apache.log4j.Logger;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

@WebServlet({ "/AddAdminKPI" })
public class AddAdminKPI extends HttpServlet
{
    private static final long serialVersionUID = 1L;
    Logger logger;

    public AddAdminKPI() {
        this.logger = Logger.getLogger(AddAdminKPI.class.getName());
    }

    protected void doGet(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        final String isManual = "manual";
        String fiware_service = null;
        String fiware_servicepath = null;
        String createdOnUpdateKPISubscription = null;
        String createdKPISubscriptionEntity = null;
        String subscriptionIdCreated = null;
        JSONObject subscrIdj = null;
        final float value = 0.0f;
        String idP = request.getParameter("id");
        String name = request.getParameter("name");
        String desc = request.getParameter("desc");
        String unit = request.getParameter("unit");
        name = name.trim();
        desc = desc.trim();
        unit = unit.trim();
        
        
        String level = "task";
        Integer id = Integer.parseInt(idP);
        String valueS = "0";
        
        String lang;
        if (request.getParameter("lang") != null) {
            lang = request.getParameter("lang");
        }
        else {
            lang = "en";
        }
        ArrayList<City> cities = new ArrayList<City>();
        final HashSet<String> notificationAttrs = new HashSet<String>();
        notificationAttrs.add("value");
        
        String category;
        
            category = request.getParameter("cat1");
       
        Integer idKPI = Queries.addCommonKPI("new common KPI", id, name, desc, unit, Integer.valueOf(0), level, category, isManual, value);
        String idKpi = idKPI.toString();
       

        cities = (ArrayList<City>)Queries.getCities();
        for (final City c : cities) {
        	String cityname = c.getName();
        	String db = cityname.toLowerCase()+"_CommonKPI"+idKpi;
        	System.out.println("DATABASE: " + db);
        	CalculateKPI.saveAsMeasure(db, name+"_common_"+c.getName(),c.getName().toLowerCase());
        	fiware_service = c.getName().toLowerCase();
            fiware_servicepath = "/" + c.getName().toLowerCase() + "_kpi/harmonized";
        
       // logger.debug(idP+ ", " + name + ", " + notificationAttrs + ", "+ fiware_service + ", "+ fiware_servicepath);
        try {
			createdOnUpdateKPISubscription = SubscriptionManager.createDeviceSubscription(idKpi.toString()+"_ManualKPI2", name, (HashSet)notificationAttrs, fiware_service, fiware_servicepath);
			subscrIdj = new JSONObject(createdOnUpdateKPISubscription);
			subscriptionIdCreated = subscrIdj.getString("subscriptionId");
			createdKPISubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(name, "number", id.toString(),idKpi.toString()+"_manual_v2", subscriptionIdCreated, "0", fiware_service, fiware_servicepath);
			Queries.updateEntityKpi(Integer.parseInt(idKpi),idKpi+"_manual2");
			
        } catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        }
        final String nextJSP = "/cpm_v2/GetAdminKPIList?city=3&level=" + level + "&category=all&lang=" + lang;
        response.sendRedirect(nextJSP);
    }

    protected void doPost(final HttpServletRequest request, final HttpServletResponse response) throws ServletException, IOException {
        this.doGet(request, response);
    }
}
