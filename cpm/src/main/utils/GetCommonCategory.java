package main.utils;

import org.apache.log4j.Logger;

public class GetCommonCategory
{
    static Logger logger;

    static {
        GetCommonCategory.logger = Logger.getLogger(GetCommonCategory.class.getName());
    }

    public String getCategory(final String c) {
        switch (c) {
            case "airquality": {
                final String category = "air quality";
                return category;
            }
            case "green": {
                break;
            }
            case "water": {
                final String category = "water management";
                return category;
            }
            case "climate": {
                final String category = "climate change adaptation and mitigation";
                return category;
            }
            case "economic": {
                final String category = "economic opportunities and green jobs";
                return category;
            }
            default:
                break;
        }
        final String category = "green space management";
        return category;
    }
}
