package main.utils;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.HttpEntity;
import org.apache.http.entity.StringEntity;
import org.apache.http.client.methods.HttpPatch;
import java.net.URI;
import org.apache.http.impl.client.HttpClients;
import javax.ws.rs.core.MediaType;
import com.sun.jersey.api.client.filter.ClientFilter;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;
import java.util.Iterator;
import java.util.Set;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.WebResource.Builder;
import java.util.Map;

public abstract class RestUtils
{
    public static String consumePost(final String url, final Object body, final Map<String, String> headers) throws Exception {
        final Client client = Client.create();
        WebResource.Builder builder = client.resource(url).type("application/json");
        if (headers != null) {
            final Set<Map.Entry<String, String>> hs = headers.entrySet();
            for (final Map.Entry<String, String> h : hs) {
                builder = (WebResource.Builder)builder.header((String)h.getKey(), (Object)h.getValue());
            }
        }
        final ClientResponse resp = (ClientResponse)builder.post((Class)ClientResponse.class, (Object)body.toString());
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        String r = "";
        if (resp.getStatus() != 204 && resp.hasEntity()) {
            r = (String)resp.getEntity((Class)String.class);
        }
        return r;
    }

    public static String consumePost(final String url, final Object body) throws Exception {
        final Client client = Client.create();
        final WebResource.Builder builder = client.resource(url).type("application/json");
        System.out.println("BODY: " + body.toString());
        final ClientResponse resp = (ClientResponse)builder.post((Class)ClientResponse.class, (Object)body.toString());
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        String r = "";
        if (resp.getStatus() != 204 && resp.hasEntity()) {
            r = (String)resp.getEntity((Class)String.class);
        }
        return r;
    }

    public static String consumeGet(final String url) throws Exception {
        final Client client = Client.create();
        final ClientResponse resp = (ClientResponse)client.resource(url).get((Class)ClientResponse.class);
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        String r = "";
        if (resp.getStatus() != 204 && resp.hasEntity()) {
            r = (String)resp.getEntity((Class)String.class);
        }
        return r;
    }

    public static String consumeGet(final String url, final HTTPBasicAuthFilter token) throws Exception {
        final Client client = Client.create();
        client.addFilter((ClientFilter)token);
        final ClientResponse resp = (ClientResponse)client.resource(url).type("application/json").get((Class)ClientResponse.class);
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        String r = "";
        if (resp.getStatus() != 204 && resp.hasEntity()) {
            r = (String)resp.getEntity((Class)String.class);
        }
        return r;
    }

    public static String consumeGet(String url, Map<String, String> headers) throws Exception {
        final Client client = Client.create();
        WebResource.Builder builder = client.resource(url).getRequestBuilder();
        if (headers != null) {
            final Set<Map.Entry<String, String>> hs = headers.entrySet();
            for (final Map.Entry<String, String> h : hs) {
                builder = (WebResource.Builder)builder.header((String)h.getKey(), (Object)h.getValue());
            }
        }
        final ClientResponse resp = (ClientResponse)builder.get((Class)ClientResponse.class);
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        String r = "";
        if (resp.getStatus() != 204 && resp.hasEntity()) {
            r = (String)resp.getEntity((Class)String.class);
        }
        return r;
    }

    public static ClientResponse consumeDelete(final String url, final Map<String, String> headers) throws Exception {
        final Client client = Client.create();
        WebResource.Builder builder = client.resource(url).getRequestBuilder();
        if (headers != null) {
            final Set<Map.Entry<String, String>> hs = headers.entrySet();
            for (final Map.Entry<String, String> h : hs) {
                builder = (WebResource.Builder)builder.header((String)h.getKey(), (Object)h.getValue());
            }
        }
        final ClientResponse resp = (ClientResponse)builder.delete((Class)ClientResponse.class);
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        return resp;
    }

    public static void consumePut(final String url, final Object body, final MediaType type, final Map<String, String> headers) throws Exception {
        final Client client = Client.create();
        WebResource.Builder builder = client.resource(url).type(type);
        if (headers != null) {
            final Set<Map.Entry<String, String>> hs = headers.entrySet();
            for (final Map.Entry<String, String> h : hs) {
                builder = (WebResource.Builder)builder.header((String)h.getKey(), (Object)h.getValue());
            }
        }
        final ClientResponse resp = (ClientResponse)builder.put((Class)ClientResponse.class, (Object)body.toString());
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
    }

    public static String consumePost(final String url, final Object body, final MediaType type, final Map<String, String> headers) throws Exception {
        final Client client = Client.create();
        WebResource.Builder builder = client.resource(url).type(type);
        if (headers != null) {
            final Set<Map.Entry<String, String>> hs = headers.entrySet();
            for (final Map.Entry<String, String> h : hs) {
                builder = (WebResource.Builder)builder.header((String)h.getKey(), (Object)h.getValue());
            }
        }
        final ClientResponse resp = (ClientResponse)builder.post((Class)ClientResponse.class, (Object)body.toString());
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        String r = "";
        if (resp.getStatus() != 204 && resp.hasEntity()) {
            r = (String)resp.getEntity((Class)String.class);
        }
        return r;
    }

    public static void consumePatch(final String url, final Object body, final Map<String, String> headers) throws Exception {
        final CloseableHttpClient httpClient = HttpClients.createDefault();
        final HttpPatch httpPatch = new HttpPatch(new URI(url));
        try {
        	
        	System.out.println("BODY: " +body.toString());
            final StringEntity entity = new StringEntity(body.toString());
            entity.setContentType("application/json");
            httpPatch.setEntity((HttpEntity)entity);
            if (headers != null) {
                final Set<Map.Entry<String, String>> hs = headers.entrySet();
                for (final Map.Entry<String, String> h : hs) {
                    httpPatch.setHeader((String)h.getKey(), (String)h.getValue());
                }
            }
            final CloseableHttpResponse response = httpClient.execute((HttpUriRequest)httpPatch);
            final int status = response.getStatusLine().getStatusCode();
            if (status > 301) {
                throw new Exception("URL " + url + " responded with status " + status);
            }
            response.close();
        }
        finally {
            httpClient.close();
        }
        httpClient.close();
    }

    public static ClientResponse consumePostFull(final String url, final Object body, final Map<String, String> headers) throws Exception {
        final Client client = Client.create();
        WebResource.Builder builder = client.resource(url).type("application/json");
        if (headers != null) {
            final Set<Map.Entry<String, String>> hs = headers.entrySet();
            for (final Map.Entry<String, String> h : hs) {
                builder = (WebResource.Builder)builder.header((String)h.getKey(), (Object)h.getValue());
            }
        }
        final ClientResponse resp = (ClientResponse)builder.post((Class)ClientResponse.class, (Object)body.toString());
        if (resp.getStatus() > 301) {
            throw new Exception("URL " + url + " responded with status " + resp.getStatus() + " and message: " + (String)resp.getEntity((Class)String.class));
        }
        return resp;
    }
    
    public static String getBody(ClientResponse resp) 
			throws Exception {

		if(resp.getStatus()!=200 && resp.getStatus()!=201 && resp.getStatus()!=301 && resp.getStatus()!=404){
			throw new Exception(" Response status is " + resp.getStatus() + " and message is " + resp.getEntity(String.class));
		}
		
		String r = resp.getEntity(String.class);
		return r;
	}
    
public static ClientResponse consumeGet2(String url, Map<String, String> headers) throws Exception{
		
		Client client = Client.create();
		WebResource webResource = client.resource(url);
		
		Builder builder =  webResource.type(MediaType.APPLICATION_JSON);
		if(headers != null){
			Set<Map.Entry<String, String>> hs = headers.entrySet();
			for(Map.Entry<String, String> h : hs){
				builder = builder.header(h.getKey(), h.getValue());
			}
		}
		
		ClientResponse resp = builder.get(ClientResponse.class);
		
		return resp;
		
	}

public static ClientResponse consumePost2(String url, Object body, Map<String, String> headers) 
		throws Exception{
	
	Client client = Client.create();
	WebResource webResource = client.resource(url);
	
	Builder builder =  webResource.type(MediaType.APPLICATION_JSON);
	if(headers != null){
		Set<Map.Entry<String, String>> hs = headers.entrySet();
		for(Map.Entry<String, String> h : hs){
			builder = builder.header(h.getKey(), h.getValue());
		}
	}
	
	ClientResponse resp =builder.post(ClientResponse.class, body.toString());
	return resp;
	
}
}
