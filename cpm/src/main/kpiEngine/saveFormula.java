package main.kpiEngine;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashSet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;
import org.json.JSONObject;

import main.java.DBConnectionEngine;
import main.java.Queries;
import main.kpiEngine.service.MySqlConnection;
import main.utils.*;

@WebServlet("/saveFormula")
public class saveFormula extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public saveFormula() {
        super();
        
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String formula = request.getParameter("formula");
		String name = request.getParameter("name");
		String city = request.getParameter("city");
		String idCPM = request.getParameter("idCPM");
		System.out.println(idCPM);
		String storageQuery;
		
		if(city.equalsIgnoreCase("1") || city.equalsIgnoreCase("2") || city.equalsIgnoreCase("3")) {
			Integer c = Integer.parseInt(city);
			city = Queries.getCityName(c);
		}
//		String storageURL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String storageUser ="root";
//		String storagePass = "mysql";
		if(idCPM.equalsIgnoreCase("0") || idCPM == null || idCPM.equalsIgnoreCase("")) {
			storageQuery = "INSERT INTO formula (name, formula, city) VALUE ('"+name+"', '"+formula+"', '"+city+"')";
			}
		
		else {
			storageQuery="INSERT INTO formula (name, formula, city, id_CPM) VALUE ('"+name+"', '"+formula+"', '"+city+"', "+idCPM+")";
			
		}
		System.out.println(storageQuery);
		Connection connection;
		Integer id = null;	
         
         Statement st=null;
         ResultSet rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
        	 //connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
 			st = connection.createStatement();
 			st.executeUpdate(storageQuery, Statement.RETURN_GENERATED_KEYS);
 			
 			
 			rs = st.getGeneratedKeys(); 
 			if(rs.next()) { id = rs.getInt(1); } 
 			
 			connection.close();
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
         
         String createdOnUpdateKPISubscription = null;
         String createdKPISubscriptionEntity = null;
         String subscriptionIdCreated = null;
         JSONObject subscrIdj = null;
         String fiware_service = city.toLowerCase();
         String fiware_servicepath = "/"+city.toLowerCase()+"_kpi/harmonized";
         final HashSet<String> notificationAttrs = new HashSet<String>();
         notificationAttrs.add("value");
        // logger.debug(idP+ ", " + name + ", " + notificationAttrs + ", "+ fiware_service + ", "+ fiware_servicepath);
         try {
			createdOnUpdateKPISubscription = SubscriptionManager.createDeviceSubscription(id.toString()+"_kpiEngine", name, (HashSet)notificationAttrs, fiware_service, fiware_servicepath);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         try {
			subscrIdj = new JSONObject(createdOnUpdateKPISubscription);
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         try {
			subscriptionIdCreated = subscrIdj.getString("subscriptionId");
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
         try {
			createdKPISubscriptionEntity = SubscriptionManager.createDeviceSubscriptionEntity(name, "number", id.toString()+"_kpiEngine" ,id.toString()+"_kpiEngine", subscriptionIdCreated, "0", fiware_service, fiware_servicepath);
			Queries.updateFormulaEntity(id.toString()+"_kpiEngine", id);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       
	}

}
