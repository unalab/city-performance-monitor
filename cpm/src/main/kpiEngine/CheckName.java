package main.kpiEngine;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.DBConnectionEngine;
import main.kpiEngine.service.MySqlConnection;

/**
 * Servlet implementation class CheckName
 */
@WebServlet("/check")
public class CheckName extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public CheckName() {
        super();
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String measure = request.getParameter("name");
//		String storageURL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String storageUser ="root";
//		String storagePass = "mysql";
		String valid = null;
		
		if(measure == "") {
			valid = "-1";
		}
		
		Connection connection;
         Statement st=null;
         ResultSet rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
 			st = connection.createStatement();
 			String query = "SELECT EXISTS(SELECT * from statement WHERE measureName=\""+measure+"\") as valid";
            st = connection.createStatement();
            rs = st.executeQuery(query);
            if (rs != null) {
                while (rs.next()) {
                    valid = rs.getString("valid");
                }
            }
            connection.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
         
       
         response.getWriter().write(valid);
	}

}
