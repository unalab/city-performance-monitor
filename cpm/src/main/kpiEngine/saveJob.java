package main.kpiEngine;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Time;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import main.java.DBConnectionEngine;
import main.kpiEngine.service.MySqlConnection;
import main.kpiEngine.service.Queries;

@WebServlet("/saveJob")
public class saveJob extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public saveJob() {
        super();
        
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String id_formula = request.getParameter("id_formula");
		String start = request.getParameter("start");
		String date = request.getParameter("date");
		String time = request.getParameter("time");
		String num = request.getParameter("num");
		String interval = request.getParameter("interval");
	
//		String storageURL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String storageUser ="root";
//		String storagePass = "mysql";
		String storageQuery;
		
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yy");
	     Date parsed = null;
	     java.sql.Date sqlDate= null;
	     Time sqltime = null;
	     
	     
		
		if(date != null) {
		
			try {
				parsed = format.parse(date);
			} catch (ParseException e) {
	
				e.printStackTrace();
			}
			sqlDate = new java.sql.Date(parsed.getTime());
			sqltime = Time.valueOf(time);
			storageQuery = "INSERT INTO job (`start`, `interval`,`date`,`time`,`num`,`id_formula`) VALUES ('"+start+"', '"+interval+"', '"+sqlDate+"', '"+sqltime+"', "+num+", "+id_formula+")";
			
		}
		else {
			
			
			storageQuery = "INSERT INTO job (`start`, `interval`,`date`,`time`,`num`,`id_formula`) VALUES ('"+start+"', '"+interval+"', "+sqlDate+", "+sqltime+", "+num+", "+id_formula+")";
		}
		System.out.println(storageQuery);
		Connection connection;
				
         
         Statement st=null;
         ResultSet rs = null;
         
         Integer retrieveID = 0;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
 			st = connection.createStatement();
 			st.executeUpdate(storageQuery);
 			
 	         if (rs != null) {
 	             while (rs.next()) {
 	                 retrieveID = rs.getInt("id");
 	             }
 	         }
 			connection.close();
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
         
         System.out.println(start);
         Integer id = Integer.parseInt(id_formula);
         if(start.equalsIgnoreCase("now")) {
        	 
        	 Queries.updateFormulaStatus(id, "PLAYED");
         }
         else {
        	 
        	 Queries.updateFormulaStatus(id, "STOPPED");
         }
         
        response.setCharacterEncoding("UTF-8");
 		response.getWriter().write(retrieveID.toString());
	}

}
