package main.kpiEngine;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONArray;
import org.json.simple.JSONObject;

import com.google.gson.Gson;

import main.kpiEngine.service.Queries;

@WebServlet("/getNewStatements")
public class getNewStatements extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public getNewStatements() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String city = request.getParameter("city");
		
		ArrayList<String> statements = new ArrayList();
		statements = Queries.getStatsName(city);
		JSONArray json = new JSONArray();
		JSONObject obj = null;
		for(int i=statements.size()-1; i>=0; i--) {
			obj=new JSONObject();
			obj.put("title", statements.get(i));
			obj.put("description", "measure");
			obj.put("frequent", true);
			obj.put("id", i+1);
			json.put(obj);
		}
		Gson gson = new Gson();
		String jsonString = gson.toJson(json);
		jsonString = jsonString.replace("{\"myArrayList\":", "");
		jsonString = jsonString.replace("{\"map\":","");
		jsonString = jsonString.replace("}]}", "]");
		jsonString = jsonString.replace("}},{", "},{");
		System.out.println(jsonString);
		response.getWriter().write(jsonString);
	}

}
