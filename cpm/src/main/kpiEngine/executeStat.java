package main.kpiEngine;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONException;

import com.google.gson.Gson;

import main.kpiEngine.service.*;

@WebServlet("/executeStat")
public class executeStat extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
 
    public executeStat() {
        super();
       
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String measure = request.getParameter("measure");
		String function = request.getParameter("fun");
		
		Stat result = Queries.execMeasure(measure);
		ArrayList<Double> d = new ArrayList<Double>();
		
		String type = result.getType();
		
		switch(type) {
		case "sql":
			
			d = Queries.getSqlValues(result.getUrl(),result.getPassword(),result.getUsername(),result.getQuery());
			
			break;
		case "influx":
			try {
				d=Queries.getInfluxValues(result.getUrl(),result.getPassword(),result.getUsername(),result.getQuery());
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break; 
		}
		
		
		Double res = Utils.executeFunction(d,function);
		String resString = new Gson().toJson(res);
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(resString);
	}


}
