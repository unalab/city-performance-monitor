package main.kpiEngine.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

//import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.quartz.JobKey;

import com.google.gson.Gson;

import main.java.DBConnectionEngine;
import main.utils.RestUtils;

public class Queries {
	private static ResultSet rs;
	private static ArrayList<String> names;

	public static ArrayList<String> getStatsName(String city) {
		
		names = new ArrayList<String>();
//		String URL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String user ="root";
//		String pass = "mysql";
		String query = "SELECT measureName FROM statement WHERE city='"+city+"'";
		System.out.println(query);
		Connection connection;
		String value = null;
         
         Statement st=null;
         rs = null;
         try {
 			//connection = MySqlConnection.createConnection(URL,user,pass);
        	 connection = DBConnectionEngine.createConnection();
 			st = connection.createStatement();
 			rs=st.executeQuery(query);
 			
 			 if (rs != null) {
 			   while(rs.next()) {
               value = rs.getString("measureName");
               
               names.add(value);
 			   }
             
 		 }     
             connection.close();   
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
        
		return names;
	}

	public static Stat execMeasure(String measure) {
		Stat s = new Stat();
		String query = "SELECT * FROM statement WHERE measureName='"+measure+"'";
//		String URL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String user ="root";
//		String pass = "mysql";
		
		System.out.println(query);
		Connection connection;
		Statement st=null;
        rs = null;
        try {
			//connection = MySqlConnection.createConnection(URL,user,pass);
        	connection = DBConnectionEngine.createConnection();
        	st = connection.createStatement();
			rs=st.executeQuery(query);
			
			 if (rs != null) {
			   while(rs.next()) {
				   s.setType(rs.getString("type"));
				   s.setUrl(rs.getString("url"));
				   s.setUsername(rs.getString("username"));
				   s.setPassword(rs.getString("password"));
				   s.setQuery(rs.getString("query"));
			   }
            
		 }     
            connection.close();   
		} catch (SQLException e) {
		
				e.printStackTrace();
			
		}
		return s;
	}

	public static ArrayList<Double> getSqlValues(String url, String password, String username, String query) {
		ArrayList<Double> d = new ArrayList<Double>();
		Connection connection;
		Statement st=null;
		ResultSetMetaData rsMetaData = null;
        rs = null;
        try {
			connection = MySqlConnection.createConnection(url,username,password);
			st = connection.createStatement();
			rs=st.executeQuery(query);
			rsMetaData = rs.getMetaData();
			String colType = rsMetaData.getColumnTypeName(1);
			String columnName =rsMetaData.getColumnName(1);
			 if (rs != null) {
			   while(rs.next()) {
				  
				   if (colType.equalsIgnoreCase("double")) {
					   System.out.println(rs.getDouble(columnName));
					   d.add(rs.getDouble(columnName));
					}
					else if (colType.equalsIgnoreCase("float")) {
						System.out.println(rs.getFloat(columnName));
						d.add((double) rs.getFloat(columnName));
					}
					else if (colType.equalsIgnoreCase("integer")) {
						System.out.println(rs.getInt(columnName));
						d.add((double) rs.getInt(columnName));
					}
			   }
            
		 }     
            connection.close();   
		} catch (SQLException e) {
		
				e.printStackTrace();
			
		}
        System.out.println(d);
		return d;
		
	}

	public static ArrayList<Double> getInfluxValues(String url, String password, String username, String query) throws MalformedURLException, JSONException {
		ArrayList<Double> res = new ArrayList<Double>();
		String dbname = new URL(url).getPath();
		url = url.replace(dbname,"");
		dbname = dbname.replace("/", "");
		
		JSONObject json = new JSONObject();
		
		query = query.replaceAll("\\n"," ");
		query = query.replace(" ","%20");
		
		String connectionString = null;
		
		
		System.out.println("query" + query);
		
		if(username != "" && password!="") {
			connectionString = url + "/query?db="+dbname+"&q="+query+"&u="+username+"&p="+password;
		}
		
		else {
			connectionString = url + "/query?db="+dbname+"&q="+query;
		}
		System.out.println(connectionString);
		String resp = null;
		try {
			resp = RestUtils.consumeGet(connectionString);
			System.out.println(resp);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		
		try {
			 json =  new JSONObject(resp);
		} catch (JSONException e) {
			
			e.printStackTrace();
		}
		
		JSONArray array = (JSONArray) json.get("results");
		JSONObject obj = (JSONObject) array.get(0);
		JSONArray array2 = (JSONArray) obj.get("series");
		JSONObject obj2 = (JSONObject) array2.get(0);
		
		JSONArray values =(JSONArray) obj2.get("values");
		System.out.println(values.toString());
			for (int i = 0; i< values.length(); i++) {
				JSONArray val = (JSONArray) values.get(i);
				System.out.println(val.get(1));
				String valore = val.get(1).toString();
				Double n = Double.parseDouble(valore);
				res.add(n);
					
				}
		
		return res;
	}
	
	static double convertDouble(Object object){
	    double valueTwo = (double)object;
	    System.out.println(valueTwo);
	    return valueTwo;
	}

	public static JSONArray getKPIs(String city) throws JSONException {
		JSONArray kpis = new JSONArray();
//		String URL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String user ="root";
//		String pass = "mysql";
		String query = "SELECT * FROM formula WHERE city='"+city+"'";
		System.out.println(query);
		Connection connection;
		String value = null;
         
         Statement st=null;
         rs = null;
         try {
 			//connection = MySqlConnection.createConnection(URL,user,pass);
        	 connection = DBConnectionEngine.createConnection();
 			st = connection.createStatement();
 			rs=st.executeQuery(query);
 			JSONObject obj; 
 			 if (rs != null) {
 			   while(rs.next()) {
 				   obj = new JSONObject();
 				   obj.put("name", rs.getString("name"));
 				   obj.put("formula", rs.getString("formula"));
 				   obj.put("status", rs.getString("status"));
 				   obj.put("id", rs.getInt("id"));
 				   kpis.put(obj);
 			   }
             
 		 }     
             connection.close();   
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
        
		return kpis;

	}
	
	public static JSONArray getCpmKPIs(String city) throws JSONException {
		JSONArray kpis = new JSONArray();
//		String URL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String user ="root";
//		String pass = "mysql";
		String query = "SELECT name FROM formula WHERE city='"+city+"' AND id_CPM IS NULL";
		System.out.println(query);
		Connection connection;
		String value = null;
         
         Statement st=null;
         rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(URL,user,pass);
 			st = connection.createStatement();
 			rs=st.executeQuery(query);
 			JSONObject obj; 
 			 if (rs != null) {
 			   while(rs.next()) {
 				   obj = new JSONObject();
 				   obj.put("name", rs.getString("name"));
// 				   obj.put("formula", rs.getString("formula"));
// 				   obj.put("status", rs.getString("status"));
// 				   obj.put("id", rs.getInt("id"));
 				   kpis.put(obj);
 			   }
             
 		 }     
             connection.close();   
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
        
		return kpis;

	}

	
	public static JSONArray getKPIs() throws JSONException {
		JSONArray kpis = new JSONArray();
//		String URL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String user ="root";
//		String pass = "mysql";
		String query = "SELECT * FROM formula";
		System.out.println(query);
		Connection connection;
		String value = null;
         
         Statement st=null;
         rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(URL,user,pass);
 			st = connection.createStatement();
 			rs=st.executeQuery(query);
 			JSONObject obj; 
 			 if (rs != null) {
 			   while(rs.next()) {
 				   obj = new JSONObject();
 				   obj.put("name", rs.getString("name"));
 				   obj.put("formula", rs.getString("formula"));
 				   obj.put("status", rs.getString("status"));
 				   obj.put("id", rs.getInt("id"));
 				   obj.put("city", rs.getString("city"));
 				   kpis.put(obj);
 			   }
             
 		 }     
             connection.close();   
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
        
		return kpis;

	}

	public static void updateFormulaStatus(Integer id_formula, String status) {
//		String storageURL = "Jdbc:mysql://217.172.12.202:8093/kpiEngine";
//		String storageUser ="root";
//		String storagePass = "mysql";
		String query = "UPDATE formula SET status='"+status+"' WHERE ID="+id_formula;
		System.out.println(query);
		Connection connection;
        Statement st=null;
        ResultSet rs = null;
        try {
        	connection = DBConnectionEngine.createConnection();
			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
			st = connection.createStatement();
			st.executeUpdate(query);
			connection.close();
		} catch (SQLException e) {
		
				e.printStackTrace();
			
		}
		
	}

	public static void updateJob(String id, String string) {
		String query = "UPDATE job SET jobKey='"+string+"' WHERE ID="+id;
		System.out.println(query);
		Connection connection;
        Statement st=null;
        ResultSet rs = null;
        try {
        	connection = DBConnectionEngine.createConnection();
			//connection = MySqlConnection.createConnection(storageURL,storageUser,storagePass);
			st = connection.createStatement();
			st.executeUpdate(query);
			connection.close();
		} catch (SQLException e) {
		
				e.printStackTrace();
			
		}
		
		
	}

	public static String getJobName(String id_job) {
		String query = "SELECT jobKey FROM job WHERE id="+id_job;
		System.out.println(query);
		String name = null;
		Connection connection;
		
         
         Statement st=null;
         rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(URL,user,pass);
 			st = connection.createStatement();
 			rs=st.executeQuery(query);
 			
 			 if (rs != null) {
 			   while(rs.next()) {
 				  name = rs.getString("jobKey");
 			   }
             
 		 }     
             connection.close();   
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
        
		return name;
	}

	public static Integer getIdFormula(String id_job) {
		String query = "SELECT id_formula FROM job WHERE id="+id_job;
		System.out.println(query);
		Integer formula = null;
		Connection connection;
		
         
         Statement st=null;
         rs = null;
         try {
        	 connection = DBConnectionEngine.createConnection();
 			//connection = MySqlConnection.createConnection(URL,user,pass);
 			st = connection.createStatement();
 			rs=st.executeQuery(query);
 			
 			 if (rs != null) {
 			   while(rs.next()) {
 				  formula = rs.getInt("id_formula");
 			   }
             
 		 }     
             connection.close();   
 		} catch (SQLException e) {
 		
 				e.printStackTrace();
 			
 		}
        
		return formula;
	}
}
