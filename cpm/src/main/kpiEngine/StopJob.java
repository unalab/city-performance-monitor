package main.kpiEngine;

import org.quartz.InterruptableJob;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.UnableToInterruptJobException;

public class StopJob implements InterruptableJob {

private volatile boolean isJobInterrupted = false;

private JobKey jobKey = null;

private volatile Thread thisThread;

public StopJob() {
}

@Override
public void interrupt() throws UnableToInterruptJobException {
    // TODO Auto-generated method stub
    System.err.println("calling interrupt:"+thisThread+"==>"+jobKey);
    isJobInterrupted = true;
    if (thisThread != null) {
        // this call causes the ClosedByInterruptException to happen
        thisThread.interrupt();
    }

}



@Override
public void execute(JobExecutionContext arg0) throws JobExecutionException {
	// TODO Auto-generated method stub
	
}





}
