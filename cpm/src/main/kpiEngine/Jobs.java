package main.kpiEngine;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.ScheduleBuilder;
import org.quartz.Scheduler;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.quartz.impl.StdSchedulerFactory;

import main.kpiEngine.service.CalculateKPI;
import main.kpiEngine.service.Queries;


@WebServlet("/jobs")
public class Jobs extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    
    public Jobs() {
        super();
        
    }

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		String id = request.getParameter("id");
		String start = request.getParameter ("start");
		String num = request.getParameter("num");
		String interval = request.getParameter("interval");
		System.out.println("JOB EXECUTION...");
		if (start.equalsIgnoreCase("now")) {
			final JobDetail job = JobBuilder.newJob((Class)CalculateKPI.class).withIdentity(id).build();
			System.out.println("KEY JOB:" +job.getKey());
			Queries.updateJob(id, job.getKey().getName());
			int seconds;
			Trigger trigger = null;
			seconds = Integer.parseInt(num);
			switch(interval) {
			case "seconds":
				
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInSeconds(seconds).repeatForever()).build();
				break;
			case "minutes":
				
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(seconds).repeatForever()).build();
				break;
			case "hours":
				
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			case "days":
				
				seconds = seconds*24;
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			case "months":
				
				seconds = seconds*24*30;
				trigger = TriggerBuilder.newTrigger().startNow().withSchedule((ScheduleBuilder)SimpleScheduleBuilder.simpleSchedule().withIntervalInHours(seconds).repeatForever()).build();
				break;
			}
			
			
			
			 
			final SchedulerFactory schFactory = (SchedulerFactory)new StdSchedulerFactory();
			Scheduler sch = null;
			try {
				sch = schFactory.getScheduler();
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sch.start();
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				sch.scheduleJob(job, trigger);
				
			} catch (SchedulerException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		doGet(request, response);
	}

}
