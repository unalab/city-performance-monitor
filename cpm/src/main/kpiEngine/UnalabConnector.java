package main.kpiEngine;

import java.io.IOException;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLClientInfoException;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.apache.commons.lang.math.NumberUtils;
import org.influxdb.InfluxDB;
import org.influxdb.InfluxDBFactory;
import org.influxdb.dto.Query;
import org.influxdb.dto.QueryResult;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonSerializer;
import com.sun.jersey.api.client.ClientResponse;

import main.kpiEngine.service.MySqlConnection;
import main.utils.*;

@WebServlet("/unalabConnect")
public class UnalabConnector extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public UnalabConnector() {
        super();
        // TODO Auto-generated constructor stub
    }

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException{
		// TODO Auto-generated method stub
		String url = request.getParameter("url");
		String password = request.getParameter("password");
		String username = request.getParameter("username");
		String query = request.getParameter("query");
		String dbname = request.getParameter("db");
		url = url.replace(dbname,"");
		dbname = dbname.replace("/", "");
		
		
		query = query.replaceAll("\\n"," ");
		query = query.replace(" ","%20");
		
		String connectionString = null;
		JSONObject json = null;
		
		System.out.println("query" + query);
		
		if(username != "" && password!="") {
			connectionString = url + "/query?db="+dbname+"&q="+query+"&u="+username+"&p="+password;
		}
		
		else {
			connectionString = url + "/query?db="+dbname+"&q="+query;
		}
		System.out.println(connectionString);
		String resp = null;
		try {
			resp = RestUtils.consumeGet(connectionString);
		} catch (Exception e) {
			
			e.printStackTrace();
		}
		try {
			 json =  (JSONObject) new JSONParser().parse(resp);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
		JSONArray array = (JSONArray) json.get("results");
		JSONObject obj = (JSONObject) array.get(0);
		JSONArray array2 = (JSONArray) obj.get("series");
		
		
		JSONObject obj2 = (JSONObject) array2.get(0);
		JSONArray columns = (JSONArray) obj2.get("columns");
		JSONArray values = (JSONArray) obj2.get("values");
		System.out.println(values.toString());
		JSONArray results = new JSONArray();
		String jsonString = null;
		
		
		int num = columns.size()-1;
		
		if (num == 1) {
			
			String value =((JSONArray) values.get(0)).get(1).toString();
			
			
			boolean isCorrect = NumberUtils.isNumber(value);
			
			
			if (isCorrect) {
			
			for (int i = 0; i< values.size(); i++) {
				JSONArray val = (JSONArray) values.get(i);
				JSONObject obj3 = new JSONObject();
			
				
					for (int j=0; j<val.size(); j++) {
						
						obj3.put(columns.get(j), val.get(j));
					
						
				
					}
					results.add(obj3);
					jsonString = new Gson().toJson(results);
				
				}
			}
			
				else {
					jsonString = "ERROR: query result must be numeric";
				}
				
		}
		
		
		else {
			jsonString = "ERROR: The query result must have only one column";
		}
		
		System.out.println(jsonString);
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonString);
	}




}
